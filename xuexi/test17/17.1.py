# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         17.ajaxdata.json
# Description:  线程与进程
# Author:       dell
# Date:         2019/5/11
#-------------------------------------------------------------------------------
# from time import sleep, ctime
# def fun1():
#     print('开始运行fun1:',ctime())
#     sleep(4)
#     print('fun1运行结束',ctime())
#
# def fun2():
#     print('开始运行fun2',ctime())
#     sleep(2)
#     print('fun2运行结束',ctime())
#
# def main():
#     print('开始运作时间：',ctime())
#     fun1()
#     fun2()
#     print('结束时间:',ctime())
#
# if __name__ == '__main__':
#     main()
# 动态的计算机程序被称为一个进程，进程是活跃的，只有可执行程序被调入内存中才称为进程
# 线程属于进程，一个进程中包括一个或者多个线程
# 使用单线程执行程序
from time import sleep ,ctime
def fun1():
    print('开始运行fun1:',ctime())
    #休眠4秒
    sleep(4)

def fun2():
    print('开始运行fun2：',ctime())
    sleep(2)
    print('fun2运行结束：',ctime())

def main():
    print('开始运行时间：',ctime())
    fun1()
    fun2()
    print('结束运行时间：',ctime())

if __name__ == '__main__':
    main()