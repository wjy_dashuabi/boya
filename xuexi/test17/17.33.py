# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         17.33
# Description:
# Author:       dell
# Date:         2019/5/25
#-------------------------------------------------------------------------------
import threading
from time import sleep,ctime

#继承子线程类
#之前存在写好的 函数
class Mythread(threading.Thread):
    #线程函数  参数，线程名称
    def __init__(self,func,args,name=''):
        super().__init__(target=func,name=name,args=args)

    # def run(self):
    #     self._target(*self._args)

#之前没有写好的线程函数
class Mythread2(threading.Thread):
    def __init__(self,sec):
        super().__init__()
        self.sec = sec
#重写父类的，run方法，里面的代码就是线程函数代码
    def run(self):
        print(self.sec)
        sleep(self.sec)
        print('run method overf')
def func(index,sec):
    print('开始执行',index,'执行时间',ctime())
    sleep(sec)
    print('执行结束', index, '结束时间', ctime())


def main():
    thread1 = Mythread(func,args=(40,4),name='thread1')
    thread1.start()
    thread2 = Mythread(func,args=(20,2),name='thread2')
    thread2.start()

    thread1.join()
    thread2.join()


if __name__ == '__main__':
    main()



