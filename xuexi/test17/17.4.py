# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         17.4
# Description:
# Author:       dell
# Date:         2019/5/11
#-------------------------------------------------------------------------------
import _thread as thread
from time import sleep,ctime
def fun(index,sec,lock):
    print('开始执行',index,'执行时间:',ctime())
    sleep(sec)
    print('执行结束',index,'执行时间;',ctime())
    lock.relaese()

def main():
    lock1 =  thread.allocate_lock()
    lock1.acquire()
    thread.start_new_thread(fun,)
