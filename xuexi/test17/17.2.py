# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         17.2
# Description:  使用多线程执行程序
# Author:       dell
# Date:         2019/5/11
#-------------------------------------------------------------------------------
# from time import sleep, ctime
# import _thread as thread
# def fun1():
#     print('开始运行fun1:',ctime())
#     sleep(4)
#     print('fun1运行结束',ctime())
#
# def fun2():
#     print('开始运行fun2',ctime())
#     print('fun2运行结束',ctime())
#
# def main():
#     print('开始运作时间：',ctime())
#     # 启动第一个线程
#     # 两个参数，第一个是函数名，第二个参数是要传入的线程函数的参数
#     thread.start_new_thread(fun1, ())
#
#     # 启动第二个线程
#     thread.start_new_thread(fun2, ())
#     sleep(6)
#     print('结束时间:',ctime())
#
# if __name__ == '__main__':
#      main()

# 使用多线程执行程序
import _thread as thread
from time import sleep ,ctime
def fun1():
    print('开始运行fun1:',ctime())
    sleep(4)
    print('fun1运行结束:',ctime())
def fun2():
    print('开始运行fun2:',ctime())
    sleep(2)
    print('fun2结束运行:',ctime())
def main():
    print('开始运行时间:',ctime())
    thread.start_new_thread(fun1,())
    thread.start_new_thread(fun2,())
    sleep(6)
    print('结束运行时间:',ctime())

if __name__ == '__main__':
    main()