# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         17.32
# Description:
# Author:       dell
# Date:         2019/5/25
#-------------------------------------------------------------------------------
import threading
from time import sleep,ctime

class Mythread:
    def __init__(self,func,args):
        self.func = func
        self.args = args

    #重写 _call_方法
    def __call__(self):
        #执行函数
        self.func(*self.args)

def func(index,sec):
    print('开始执行',index,'执行时间',ctime())
    sleep(sec)
    print('执行结束', index, '结束时间', ctime())


def main():
    thread1 = threading.Thread(target=Mythread(func,args=(40,4)))
    thread1.start()

    thread2 = threading.Thread(target=Mythread(func, args=(10, 2)))
    thread2.start()

    thread1.join()
    thread2.join()

if __name__ == '__main__':
    main()

