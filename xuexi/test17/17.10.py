# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         17.10
# Description:
# Author:       dell
# Date:         2019/5/25
#-------------------------------------------------------------------------------
from atexit import register
from random import randrange
from threading import BoundedSemaphore,Lock,Thread
from time import sleep,ctime

lock = Lock()
MAX = 5
candytray = BoundedSemaphore(MAX)

def refill():
    lock.acquire()
    print('重新添加糖果...',end=' ')
    try:
        candytray.release()
    except:
        print('糖果机满了,无须添加')
    else:
        print('成功添加糖果')

    lock.release()

def buy():
    lock.acquire()
    print('购买糖果...',end=' ')

    if candytray.acquire(False):
        print('成功购买糖果')
    else:
        print('糖果机为空,无法购买')

    lock.release()

def producer(loops):
    for i in range(loops):
        refill()
        sleep(randrange(3))

def consumer(loops):
    for i in range(loops):
        buy()
        sleep(randrange(5))

def main():
    print('开始: ',ctime())
    nloops = randrange(2,4)
    print('糖果机共有 %d 个槽:' % MAX)

    Thread(target=consumer,args=(randrange(nloops,nloops+MAX+2),)).start()
    Thread(target=producer, args=(nloops,)).start()

@register
def exit():
    print('程序执行完毕: ',ctime())

if __name__ == '__main__':
    main()