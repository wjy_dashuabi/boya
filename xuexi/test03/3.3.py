# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         3.3
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/20
#-------------------------------------------------------------------------------
from click._compat  import  raw_input
name = raw_input('请输入您的名字:')
if name.startswith('F'):
    print('名字以F开头')
elif name.startswith('B'):
    print('名字以B开头')
elif name.startswith('T'):
    print('名字以T开头')
else:
    print('名字以其他字母开头')

