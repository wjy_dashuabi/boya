# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         3.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/20
#-------------------------------------------------------------------------------
print('name=','BILL')
print('age=',30)
#三种不同的输出方法所得到的效果对比
print('Apple'+','+'Orange'+','+'banana')
print('apple','Orange','banana',sep=',')
print('Apple','Orange','banana')
print('can','you','tell','how','to','get','to','the','nearest','tube','station',sep='&')
# 修改输出字符串结尾符为长度为0的字符串，这样下一次调用print函数，输出的内容就在同一行
print('hello',end='')
print('world')
print('a',end='');
print('b',end='');
print('c',end='');

