# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         3.7
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/26
#-----------------------------------------------------------------------------
# while 循环
x = 1
while x <=  10:
    print(x)
    x += 1
#for 循环
numbers = [1,2,3,4,5,6]
for number in numbers:
    print(number,end=' ')

# 用while循环输出1~10
x = 1
while x <= 10:
    print(x)
    x += 1

# 定义一个列表
css = [1,2,3,4,5,6,7,8,9,10]
print('\n用for循环输出表中的值')
for cs in css :
    print(cs,end=' ')
print('\n用for循环输出列表中的值（ajaxdata.json~9999）')
numbers = range(1,10000)
for number in numbers :
    print(number,end=' ')
print('\n用forr循环输出列表中的值的乘积')
for num  in numbers:
    print(num*num,end=' ')

