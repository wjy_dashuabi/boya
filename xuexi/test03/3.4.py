# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         3.4
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/21
#-------------------------------------------------------------------------------
name = input('请问您叫什么名字：')
if name.startswith('Bill'):
    if name.endswith('Gates'):
        print('欢迎 Bill Gates先生')
    elif name.endswith('Clinton'):
        print('欢迎克林顿先生')
    else:
        print('未知姓名')
elif name.startswith('李'):
    if name.endswith('宁'):
        print('欢迎李宁老师')
    else :
        print('未知姓名')
else:
    print('未知姓名')




