# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         3.8
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/26
#-------------------------------------------------------------------------------
x =  0
while x < 100 :
    if  x == 5 :
        break ;
    print(x,end='')
    x += 1
names = ['Bill','Mike','Mary']
print('\nbreak语句在for循环中的应用')
for name in names:
    if  not name.startswith('B'):
        break;
        print(name)

print('break语句在for循环中应用')
for name in names:
    if  name.startswith('B'):
        continue
    print(name,end=' ')
print('\n嵌套模型')
arr1 = [1,2,3,4,5]
arr2 = ['Bill','Mike','Mary']
arr3 = [arr1,arr2]
i = 0
while i < len(arr3):
    for value in arr3[i]:
        print(value,end=' ')
    i += 1
    print()
print(arr3)