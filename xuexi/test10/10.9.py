# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.9
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 静态方法和类方法
class MyClass :
     name ="Bill"
     def   __init__(self):
         print('MyClass的构造方法被调用')
         self.value =20
     @staticmethod
     def  run():
         print('*',MyClass.name,'*')
         print("MyClass的静态方法run被调用")
     @classmethod
     def do(self):
         print('[',self.name,']')
         print('调用静态方法run')
         self.run()
     def dol(self):
         print(self.value)
         print('<',self.name,'>')
         print(self)
MyClass.run()
c= MyClass()
c.do()
print('MyClass2.name','=',MyClass.name)
MyClass.do()
c.dol()