# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         1061
# Description:
# Author:       dell
# Date:         2019/4/3
#-------------------------------------------------------------------------------
nestedList = [[1,2,3],[4,3,2],[1,2,4,5,7]]

def enumList(nestedList):
    for subList in nestedList:
        for element in subList:
            yield element


# 对生成器函数进行迭代
for num in enumList(nestedList):
    print(num,end=' ')

print()
# 将生成器函数转换成列表
numList = list(enumList(nestedList))
print(numList)
print(numList[1:5])

#双重循环 生产器
def enumlist(nestedList):
    for subList in nestedList:
        for element in subList:
            yield element

import time
#对生成器函数进行迭代
for num in enumList(nestedList):
    print(num,end='')
    time.sleep(0.1)

