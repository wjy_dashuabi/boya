# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.4、
# Description:
# Author:       dell
# Date:         2019/3/4
#-------------------------------------------------------------------------------
# 特殊成员方法
# _len_(self):返回序列中元素的个数
# _getitem_(self,key):返回与所给键对应的值
# _setitem_(self,key,value):设置key的值
# _delitem_():从序列中删除键为key的key-value对。
class FactorialDict:
    def __init__(self):
        self.numDict = {}
    def factorial(self,n):
        if n== 0 or n== 1:
            return 1
        else:
            return n * self.factorial(n - 1 )
    def __getitem__(self, key):
        print('_getitem_方法被调用，key = {}'.format(key))
        if  key in self.numDict:
            return self.factorial(self.numDict[key])
        else:
            return 0
    def __setitem__(self, key, value):
        print('_setitem_方法被调用，key={}'.format(key))
        self.numDict[key] = int (value)
    def __delitem__(self, key):
        print('_delitem_方法被调用，key={}'.format(key))
        del self.numDict[key]
    def __len__(self):
        print('_len_方法被调用')
        return len(self.numDict)
d = FactorialDict()
d['4!'] =4
d['7!'] = 7
d['12!'] =12
print('4!','=',d['4!'])
print('7!','=',d['7!'])
print('12!','=',d['12!'])
print('len','=',len(d))
del d['7!']
print('7!','=',d['7!'])
print('len', ' = ',len(d))
