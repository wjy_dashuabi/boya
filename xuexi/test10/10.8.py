# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.8
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 监控对象中所有的属性
# 有错误并未能找出!!!!!
class Wjy:
    def __init__(self):
        self.width = 0
        self.heigt = 0
        self.left = 0
        self.top = 0
    def __setattr__(self,name , value):
        print('{}被设置，新值为{}'.format(name,value))
        if name == 'size':
            self.width,self.heigt = value
        elif name == 'position':
            self.left,self.top = value
        else:
            self.__dict__[name]= value
    def __getattr__(self, name):
        print('{}被获取'.format(name))
        if name == 'size':
            return self.width,self.heigt
        elif name == 'position':
            self.left,self.top  = 0,0
r = Wjy()
r.size = 300,500
r.position = 100,400
print('size','=',r.size)
print('position','=',r.position)
del r.size,r.position
print(r.size)
print(r.position)