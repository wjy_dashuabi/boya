
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.7
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# property 函数：第一个参数需要指定用于监控读属性值的方法
#                第二个参数需要指定用于监控写属性值的方法
#                第三个参数需要指定用于删除该属性时调用的方法                                                           反正这不三不四的年纪，谁也不会只为谁着迷
class wjy:
    def __init__(self):
        self.left =  0
        self.top = 0
    def setPosition(self,position):
        print('setposition方法被调用')
        self.left,self.top = position
    #     用于监控position的读属性
    def getPosition(self):
        print('getprosition方法被调用')
        return self.left,self.top
    def deletePosition(self):
        print('position属性已被删除')
        self.left = 0
        self.top = 0#重新初始化它们的值
    position  = property(getPosition,setPosition,deletePosition)
r = wjy()
r.left = 10
r.top = 20
print('left:','=',r.left)
print('top','=',r.top)
print('postion:','=',r.position)
r.position = 100,200
print('position','=',r.position)
del  r.position
print(r.position)
r.position = 30,40
print('r.position','=',r.position)