# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         1032
# Description:
# Author:       dell
# Date:         2019/3/27
#-------------------------------------------------------------------------------
class Rectangle:
    def __init__(self):
        self.left = 0
        self.top = 0

    def setPosition(self,position):
        print('修改属性值')
        self.left,self.top = position

    def getPosition(self):
        print('获取属性值')
        return self.left,self.top

    def delPosition(self):
        print('清除属性值')
        self.left = 0
        self.top = 0

    position = property(getPosition,setPosition,delPosition)


r = Rectangle()
r.left = 10
r.top = 20
print(r.position)
r.position = 100,200
print(r.position)
del r.position
r.position = 30,40
print(r.position)

