# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.6
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 属
# 在python语言中，如果要为类添加属性，需要在构造方法(_init_)中通过self添加。
# class MyClass:
#     def  __init__(self):
#         self.value =  0
# c = MyClass
# c.value =20
# print(c.value)
# class MyClass:
#     def __init__(self):
#         self.value = 0
#     def getvalue(self):
#         print("value属性的值已经被读取")
#         return  self.value
#     def setvalue(self,value):
#         print('value属性的值已经被修改')
#         self.value = value
# c = MyClass()
# c.value =20
# c.setvalue(100)
# print('getvalue:',c.getvalue())
# print('value:',c.value)
class WJy:
    def __init__(self):
        # 同时设置left和top属性的值，position参数值应该是元组或列表类型
        self.left = 0
        self.top = 0
    def setPosition(self,position):
        self.left,self.top = position
    def getPosition(self):
        return self.left,self.top
r = WJy()
r.left=10
r.top =20
print('left:','=',r.left)
print('top:','=',r.top)
r.setPosition((30,50))
print('position:','=',r.getPosition())