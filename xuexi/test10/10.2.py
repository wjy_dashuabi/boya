# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.2
# Description:
# Author:       dell
# Date:         2019/3/4
#-------------------------------------------------------------------------------
# 重写普通方法和构造方法
class A:
    def __init__(self):
        print("A类的构造方法")
    def WJy(self):
        print('A类的WJy方法')
class B(A):
    def __init__(self):
        print("B类的构造方法")
    def Wjy(self):
        print('B类的Wjy方法')
b = B()
b.WJy()
# 创建B对象，实际上是调用B类中的构造方法
# 创建B对象，以及调用WJy方法，都是调用B类本身的方法

class Bird:
    def __init__(self):
        self.hungry = True
    def eat(self):
        if self.hungry:
            print('已经吃了虫子！')
            self.hungry = False
        else:
            print('已经吃过饭了，不饿了！')
b = Bird()
b.eat()
b.eat()

class SongBird():
    def __init__(self):
        self.sound = '在向天借五百年'
    def sing(self):
        print(self.sound)
sb = SongBird()
sb.sing()
# sb.eat()  这样会报错
# 调用了Bird类中的eat方法,没有hungry变量，所以抛出异常