# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.5
# Description:
# Author:       dell
# Date:         2019/3/4
#-------------------------------------------------------------------------------
# 从内建列表,字符串和字典继承
class Wjy1(list):
    def __init__(self,*args):
        super().__init__(*args)
        self.counter = 0
    def __getitem__(self, index):
        self.counter += 1
        return  super(Wjy1, self).__getitem__(index)
c = Wjy1(range(10))
print(c)
c.reverse()
print(c)
del c [2:7]
print(c)
print(c.counter)
print(c[1]+ c[2])
print(c.counter)

class Wjy2(dict):
    def __init__(self,*args):
        super().__init__(*args)
        self.counter = 0
    def __getitem__(self, key):
        self.counter += 1
        return super(Wjy2, self).__getitem__(key)
d = Wjy2({'name':'Bill'})
print(d['name'])
print(d.get('age'))
d['age']=30
print(d['age'])
print(d.counter)

class WJy3(str):
    def __new__(cls, *args,sep= ''):
        s = ''
        for arg in args:
            s += arg + sep
        index = -len(sep)
        if index == 0 :
            index = len(s)
        return str.__new__(cls,s[:index])
    def __init__(self,*args,sep= ''):
        pass
cs1 = WJy3('a','b','c')
cs2 = WJy3('a','b','c',sep=',')
cs3 = WJy3('a','b','c',sep='')
print('['+ cs1 +']')
print('['+ cs2 +']')
print('['+ cs3 +']')