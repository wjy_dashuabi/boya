# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.12
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
nestedList =[[1,2,3],[4,3,2],[1,2,4,5,7]]
def enumList(nestedList):
    for subList in nestedList:
        for element in subList:
            yield element
for num in enumList(nestedList):
    print(num,end=' ')
print()
numList = list(enumList(nestedList))
print(numList)
print( numList[1:4])
