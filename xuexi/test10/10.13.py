# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.13
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 递归生成器
def enumList(nestedList):
    try:
        for subList in nestedList:
            # 将多维列表中的每一个元素传入ennumList函数，如果该元素是一个列表，那么会继续迭代，
            # 否则会抛出TupeError异常，在异常处理代码中直接通过yield语句返回这个普通元素值
            # 这个异常也是递归的终止条件
            for element in enumList(subList):
                yield element
    except TypeError:
        yield nestedList
nestedList =[4,[1,2,[3,5,6]],[4,3,[1,2,[4,5]],2],[1,2,4,5,7]]
for num in enumList(nestedList):
    print(num,end=' ')
