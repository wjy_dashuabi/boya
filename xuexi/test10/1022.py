# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         1022
# Description:
# Author:       dell
# Date:         2019/3/27
#-------------------------------------------------------------------------------
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:        博智科技
# Name:         demo1022
# Description:
# Author:       yzl
# Date:         2019-02-07
#-------------------------------------------------------------------------------


# 定义一个从 list 继承的类
class CounterList(list):
    def __init__(self,*args):
        super().__init__(*args)
        self.counter = 0

    def __getitem__(self, index):
        self.counter += 1
        #
        return super(CounterList,self).__getitem__(index)

c = CounterList(range(10))
print(c)


# 定义一个从 dict 继承的类
class CounterDict(dict):
    def __init__(self,*args):
        super().__init__(*args)
        self.counter = 0

    def __getitem__(self, item):
        self.counter += 1
        # 调用父类的counter值，进行+=1的操作
        return super(CounterDict,self).__getitem__(item)

d = CounterDict({'name':'yzl'})
print(d)

# 定义一个从 str 继承的类
class MultiString(str):
    def __new__(cls, *args, sep = ' '):
        s = ''
        for arg in args:
            s += arg + sep
        index = -len(sep)

        if index == 0:
            index = len(s)

        return str.__new__(cls,s[:index])

    def __init__(self,*args, sep = ' '):
        pass


cs1 = MultiString('a','b','c',sep='==')
print(cs1)
print(len(cs1))