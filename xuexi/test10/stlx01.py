# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         stlx01
# Description:
# Author:       dell
# Date:         2019/3/23
#-------------------------------------------------------------------------------
class person:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex

if __name__ == '__main__':
    p = person('张三',21,'男')
    print(p)
    print(p.name)
