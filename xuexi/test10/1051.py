# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         1051
# Description:
# Author:       dell
# Date:         2019/4/3
#-------------------------------------------------------------------------------
class RigthTriangle:
    def __init__(self):
        self.n = 1

    def __next__(self):
        result = '*' * (2 * self.n - 1)
        self.n += 1
        return result

    def __iter__(self):
        return self


rt = RigthTriangle()
print(rt)#还没有开始迭代，就不能使用len()来检查长度
for e in rt:
    if(len(e) > 20):
        break
    print(e)

