# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.11
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 将迭代器转换为列表
class Fib:
    def __init__(self):
        self.a = 0
        self.b = 1
    def __next__(self):
        result = self.a
        self.a  , self.b = self.b , self.a+self.b
        if  result>500:raise StopIteration
        return result
    def __iter__(self):
        return self
fibs1 = Fib()
print(list(fibs1))
fibs2 =Fib()
for fib in fibs2:
    print(fib,end=' ')