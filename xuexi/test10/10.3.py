
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.3
# Description:
# Author:       dell
# Date:         2019/3/4
#-------------------------------------------------------------------------------
# 使用super函数
class Animal:
    def __init__(self):
        print('Animal init')
class Bird(Animal):
    def __init__(self,hungry):
        super().__init__()
        self.hungry = hungry
    def eat(self):
        if  self.hungry:
            print('已经吃虫子了!')
            self.hungry = False
        else:
            print('已经吃过饭了')
b = Bird(False)
b.eat()
b.eat()

class SongBird(Bird):
    def __init__(self,hungry):
        super(SongBird,self).__init__(hungry)
        self.sound = '1233'
    def sing(self):
        print(self.sound)
sb = SongBird(True)
sb.sing()
sb.eat()