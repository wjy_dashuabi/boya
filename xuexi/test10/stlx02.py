# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         stlx02
# Description:
# Author:       dell
# Date:         2019/3/23
#-------------------------------------------------------------------------------
class  Person:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex
        print('init方法被调用了')

    def eat(self):
        print('吃东西')

class Student(Person):
    def __init__(self,name,sex,age,college):
        super().__init__(name,age,sex)  #调用父类的方法，子类就不用再写了
        self.college = college

    def eat(self,apple):
        super().eat()
        print(apple)
class Student(Person):
    def __init__(self,name,age,sex,collega):
        super().__init__(name,age,sex)
    def love(self):
        print('woxihuanlixin')
if __name__ == '__main__':
    p = Person('zs',12,'boy')
    print(p )
    print(p.age)
    p.eat()               # print(p.eat()) 会输出一个None
    print('--------------------------')
    s = Student('ls',20,'girl','hut')
    s.eat('grape')

