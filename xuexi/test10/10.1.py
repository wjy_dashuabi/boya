# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/3/4
#-------------------------------------------------------------------------------
# 构造方法
# 构造方法的基础知识
class Person:
    def __init__(self,name = 'Bill'):
        print('构造方法已经被调用')
        self.name = name
    def getname(self):
        return  self.name
    def setname(self,name):
        self.name= name

person = Person()
print(person.getname())
person1 = Person(name='Mike')
print(person1.getname())
person1.setname(name = 'Jhon')
print(person1.getname())