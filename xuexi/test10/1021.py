# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         1021
# Description:
# Author:       dell
# Date:         2019/3/27
#-------------------------------------------------------------------------------
class FactorialDict:
    def __init__(self):
        self.numDict = {}

    def factorial(self,n):
        # 递归调用的终止条件
        if n == 0 or n == 1:
            return 1
        else:
            return n * self.factorial(n - 1)

    def __getitem__(self, key):
        # 遍历 key
        print("__getitem__方法被调用,key={}".format(key))
        if key in self.numDict:

            return self.factorial(self.numDict[key])
        else:
            return 0

    def __setitem__(self, key, value):
        # 设置键值
        print("__setitem__方法被调用,key={}".format(key))
        self.numDict[key] = int(value)

    def __delitem__(self, key):
        # 删除键值
        print("__delitem__方法被调用,key={}".format(key))
        del self.numDict[key]

    def __len__(self):
        # 返回字典长度
        print("__len__被调用")
        return len(self.numDict)


d = FactorialDict()

d['4!'] = 4
d['7!'] = 7

print('4!','=',d['4!'])
print('7!','=',d['7!'])
print(len(d))
del d['7!']
print(len(d))

