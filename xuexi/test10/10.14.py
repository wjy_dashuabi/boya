
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         10.14
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
def enumList(nestedList):
    try:
        try: nestedList + ''#如果nestedList不是字符串类型，会抛出异常
        except TypeError:
            pass
        else:
            raise TypeError
        for subList in nestedList:
            for element in enumList(subList):#此处调用递归
                yield element
    except TypeError:
        yield nestedList
nestedList = ['a',['b',['c'],20,123,[['hello world']]]]
for num in enumList(nestedList):
    print(num,end=' ')

