# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         7.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/3/12
#-------------------------------------------------------------------------------
list = []

while True:
    n = input('请输入一个数:')
    if n == 'exit':
        break
    n = int(n)
    list.append(n)
print(list)

def sortNumbers(*number,type = 'asc'):
    if type == 'asc':
        return sorted(number)
    else :
        return sorted(number,reverse = True)


if __name__ == '__main__':
    print(sortNumbers(*list, type='desc'))
    print(sortNumbers(*list, type='asc'))
