# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.9
# Description:
# Author:       dell
# Date:         2019/3/17
#-------------------------------------------------------------------------------
# 双端队列
from collections import deque
# 创建双端队列
q = deque(range(10))
print(q)

# 对尾部添加数据
q.append(100)
q.append(-100)
print(q)

# 对头部添加数据
q.appendleft(20)
print(q)

# 弹出尾部数据
print(q.pop())
print(q)
# 弹出头部数据
print(q.popleft())
print(q)

#将列队中的元素想左循环移动两个数
q.rotate(-2)
print(q)

#将列队中的元素向右移动4个位置
q.rotate(4)
print(q)

#创建一个队列
q1 = deque(['a','b'])

#队列追加队列
q.extend(q1)
print(q)

#头部追加队列
q.extendleft(q1)
print(q)