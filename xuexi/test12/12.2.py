# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.2
# Description:
# Author:       dell
# Date:         2019/3/12
#-------------------------------------------------------------------------------
# os模块
import os
print('当前工作目录：',os.getcwd())
print('工作目录中包含的文件后文件夹名字的列表')
#遍历指定文件
print(os.listdir(os.getcwd()))
#改变路径
#os.sep linux '/' windows'\'
os.chdir('../')
print('改变后的工作目录：',os.getcwd())
print('新的工作目录中包含的文件后文件夹名字的列表')
print(os.listdir(os.getcwd()))

print('-------------------------------')
print(os.path.abspath(__file__))
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
print(BASE_DIR)