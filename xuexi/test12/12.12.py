# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.12
# Description:
# Author:       dell
# Date:         2019/3/19
#-------------------------------------------------------------------------------
import  time
time1 = time.time()
time2 = time1 + 60
# 当前时间戳往后移一分钟
time3 = time2 + 60*60
# 当前时间戳往后移一小时
time4 = time3 + 60*60*24
time1 = time.localtime(time1)
time2 = time.localtime(time2)
time3 = time.localtime(time3)
time4 = time.localtime(time4)
print(time.strftime('%Y-%M-%D %H:%M:%S',time1))
print(time.strftime('%Y-%M-%D %H:%M:%S',time2))
print(time.strftime('%Y-%M-%D %H:%M:%S',time3))
print(time.strftime('%Y-%M-%D %H:%M:%S',time4))
#将上面4行代码分别输出前面的4个时间元组格式化后的日期和时间