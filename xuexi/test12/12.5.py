# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.5
# Description:
# Author:       dell
# Date:         2019/3/13
#-------------------------------------------------------------------------------
import os
import subprocess
#路径分隔符
print(os.sep)
#环境变量的分隔符
print(os.pathsep)
#系统名称
print(os.name)
#环境变量的值，获取path值
print(os.environ['PATH'])
print(os.getenv('PATH'))
output = subprocess.getstatusoutput('exe')
print(output)
print(os.putenv('PATH',os.getenv('PATH')+os.pathsep+'/temp'))
output = subprocess.getstatusoutput('exe')
print(output)

#执行一条外部命令  window dir
