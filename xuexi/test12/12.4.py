# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.4
# Description:
# Author:       dell
# Date:         2019/3/13
#-------------------------------------------------------------------------------
# 软连接与硬链接
import os
if os.path.exists('data.txt') and not os.path.exists('slink.txt'):
    os.symlink('data.txt','slink.txt')
if os.path.exists('data.txt') and not os.path.exists('link.txt'):
    os.link('data.txt',';link.txt')
