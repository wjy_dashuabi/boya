# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.10
# Description:
# Author:       dell
# Date:         2019/3/17
#-------------------------------------------------------------------------------
# time模块
import time
localtime  = time.localtime(time.time())
print('本地时间为：',localtime)#时间戳主要是为了提高程序的性能
print('年','=',localtime.tm_year)
print('月','=',localtime.tm_mon)
print('日','=',localtime.tm_mday)
print('一年的第%d天'%localtime[7])
localtime = time.asctime( time.localtime(time.time()))#国际标准时间格式
print('本地时间为：',localtime)



