# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.8
# Description:
# Author:       dell
# Date:         2019/3/17
# -------------------------------------------------------------------------------
# 堆
from heapq import *
from random import *
data = [1,2,3,4,5,6,7,8,9]
heap = []
for n in data :
    value = choice(data)
    # 利用choice函数从data列表中随机选择9个数（可能有重复）
    heappush(heap,value)
print(heap)
heappush(heap,2.5)
# 将2.5添加进堆
print(heap)
print(heappop(heap))

data1= [6,3,1,12,8]
heapify(data1)
print(data1)
heapreplace(data1,100)
# 弹出最小值，并将100添加进堆
print(data1)
print(nlargest(2,data1))
# 得到data1中最大的前两个值
print(nsmallest(3,data1))
print(list(merge([1,3,1234,7],[0,2,4,8],[5,10,15,20],[],[25])))