# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.3
# Description:
# Author:       dell
# Date:         2019/3/12
#-------------------------------------------------------------------------------
import os
if not os.path.exists('newdir1'):
    #创建文件夹
   os.mkdir('newdir1')
if not os.path.exists('newdir2'):
    os.mkdir('newdir2',0o377)
os.makedirs('x/y/z',0o733,True)
try:
    #删除文件夹
    os.rmdir('newdir1')
except OSError as e :
    print(e)

os.removedirs('x/y/z')
if  not os.path.exists('mydir'):
    #创建文件夹
    os.mkdir('mydir')
    #重命名
    os.rename('mydir','yourdir')
if os.path.exists('text.txt'):
    os.rename('test.txt','data.txt')
if os.path.exists('bill/mike/john'):
    os.rename('bill/mike/john','ok1/ok2/ok3')
if os.path.exists('a/aa.txt'):
    os.rename('a/aa.txt','b/bb.txt')
    os.remove('b/bb.txt')