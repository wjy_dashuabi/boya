# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.15
# Description:
# Author:       dell
# Date:         2019/3/19
#-------------------------------------------------------------------------------
# 随机数(random模块)
import random
print(random.randint(1,100))
# 在1~100中产生一个随机的整数
print(random.random())
# 从0~1中产生一个随机数
print(random.randrange(1,20,3))
# 步长为3
print((random.uniform(1,100.5)))
# 产生一个从1~100的随机浮点数
intList = [1,2,3,4,5,6,7,8,9,'a','b','c','d']
print(random.choice(intList))
newList = random.sample(intList,3)
print(newList)
random.shuffle(intList)
print(intList)

#斗地主 生成牌
#要求 ajaxdata.json 号 的牌最好