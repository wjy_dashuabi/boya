# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.16
# Description:
# Author:       dell
# Date:         2019/3/19
#-------------------------------------------------------------------------------
# 数学模块(math模块)
import math
print('圆周率','=',math.pi)
print('自然常熟','=',math.e)
print(math.fabs(-1.0))
# 取绝对值
print(math.ceil(1.3))
# 向上取整
print(math.floor(1.3))
# 向下取整
print(math.pow(2,10))
# 取2的10次幂
print(math.sin(math.pi/2))
# 计算正弦
print(math.cos(math.pi))
print(math.tan(math.pi/4))
