# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.13
# Description:
# Author:       dell
# Date:         2019/3/19
#-------------------------------------------------------------------------------
# 计算日期和时间的差值
import datetime
d1 = datetime.datetime(2017, 4, 12)
d2 = datetime.datetime(2018, 12, 25)
print((d2 - d1).days)
# 计算这两个日期之间的天数

d1 = datetime.datetime(2017, 4, 12,10,10,10)
d2 = datetime.datetime(2018, 12, 25,10,10,40)
# 定义带时间的日期
print(d2 - d1)
print((d2 - d1).seconds)
d1 = datetime.datetime.now()
# 获取当期的时间(datetime类型)
d2 = d1 + datetime.timedelta(hours=10)
# 将当前时间往后延10小时
import time
d2 = time.localtime(d2.timestamp())
print(time.strftime('%Y-%m-%d %H:%H:%M:%S',d2))