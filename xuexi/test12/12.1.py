# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/3/11
#-------------------------------------------------------------------------------
# 常用模块
# sys模块
import  sys
sys.path.append('test')
import my
my.greet('Bill')

print(sys.modules['my'])

#获取操作系统的名称
print(sys.platform)

print(sys.argv[0])

#判断程序的参数
if  len(sys.argv) == 2:
    print(sys.argv[1])
    my.greet(sys.argv[1])
s= sys.stdin.read(6)
print(s)
sys.stdout.writelines('hello world')
print()
sys.stderr.writelines('error')
sys.exit(123)
