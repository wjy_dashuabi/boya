# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         fxy
# Description:
# Author:       dell
# Date:         2019/4/13
#-------------------------------------------------------------------------------
import random
class Doudizhu:
    #定义一个斗地主类,包含一个构造方法，洗牌方法，发牌方法以及抢地主方法
    def __init__(self):
        #定义一个构造方法,描述了一副扑克牌
        self.a = '方片'
        self.b = '红桃'
        self.c = '黑桃'
        self.d = '梅花'
        self.poker1 = ['2','3','4','5','6','7','8','9','10','J','Q','K','A']
        self.poker = []
        for i in range(len(self.poker1)):
            self.poker.append(self.a + self.poker1[i])
            self.poker.append(self.b + self.poker1[i])
            self.poker.append(self.c + self.poker1[i])
            self.poker.append(self.d + self.poker1[i])
        self.poker.append('小王')
        self.poker.append('大王')



    def Xipai(self):
        #定义一个方法，洗牌
        random.shuffle(self.poker)

    def Fapai(self):
        #定义发牌方法
        self.p1 = ['大王','小王','黑桃2','红桃2','方片2','梅花2','黑桃A','红桃A','方片A','梅花A',
                   '黑桃K','红桃K','方片K','梅花K','黑桃Q','红桃Q','方片Q']
        self.poker = list(set(self.poker)^set(self.p1))
        self.p2 = random.sample(self.poker,17)
        self.poker = list(set(self.poker)^set(self.p2))
        self.p3 = random.sample(self.poker,17)
        self.poker = list(set(self.poker)^set(self.p3))
        self.p4 = self.poker


    def Qiangdizhu(self):
        #定义抢地主方法
        self.dizhu = random.randrange(1,3)
        if self.dizhu == 1:
            self.p1 = self.p1 + self.p4
        elif self.dizhu == 2:
            self.p2 = self.p2 + self.p4
        else:
            self.p3 = self.p3 + self.p4
        self.player1 = self.p1
        self.player2 = self.p2
        self.player3 = self.p3

if __name__ == '__main__':
    doudizhu = Doudizhu()
    doudizhu.Xipai()
    doudizhu.Fapai()
    doudizhu.Qiangdizhu()
    print("地主：",doudizhu.dizhu)
    print("玩家一：",doudizhu.player1)
    print("玩家二：",doudizhu.player2)
    print("玩家三：",doudizhu.player3)


