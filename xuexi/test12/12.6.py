
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         12.6
# Description:
# Author:       dell
# Date:         2019/3/13
#-------------------------------------------------------------------------------
# 集合、堆额双端队列
set1 =set(range(10))
print(type(set1))
print(set1)

# 集合的互异性 无序性
set2 = set('hello')
print(set2)


set3 = set(['Bill','wjy','Mike','wjy'])
print(set3)

a = set((1,2,3))
b = set([3,5,1,7])
# 合并集合
print(a.union(b))
print(a|b)


# 得出a b的交
print(a.intersection(b))
print(a&b)


c = set([2,3])
print(c.issubset(a))
# 判断c是否为a的子集
print(a.issubset(c))
# 判断a是否为c的子集
print(c.issuperset(a))
print(a.issuperset(c))
# 判断c是否为a的超集
d = set([1,2,3])
print(a == d)
# 判断a和d集合是否相等
print(a.difference(b))
# 计算a和b的差值，a和b的差值就是在a中删除b中存在的元素
print(a-b)
print(a.symmetric_difference(b))
# 计算a和b的对称差
print(a^b)
# 计算对称差
print((a-b)|(b-a))
x = a.copy()
print(x is a )

#向集合中添加元素
x.add(30)
print(x)
print(a)
print(d)

#判断一个值是否属于集合
print(1 in d)
print(10 in d)