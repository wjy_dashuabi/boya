# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         7.3
# Description:
# Author:       dell
# Date:         2019/2/20
#------------------------------------------------------------------------------
# 参数的值传递
# a = ajaxdata.json
# b = 2
# print('外部的a',id(a))# id()取变量在内存中的地址
# def add(a,b):
#     print('参数a',id(a))
#     print('参数b',id(b))
#     a = a+ajaxdata.json
#     b = b+ajaxdata.json
#     print("add函数内部:")
#     print('a=',a)
#     print('b=',b)
# add(a,b)
# print('内部a',id(a))


# 参数的引用传递
#引用传递地址一样
l1 = [1,2]
def add2(list):
    print('add2函数的内部:')
    print('内部',id(list))
    list[0] = list[0]+10
    list[1] = list[1]+20
    print(list)

add2(l1)
print('add2函数的外部')
print('外部',id(list))
print(l1)