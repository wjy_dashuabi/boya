# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         随堂练习4
# Description:
# Author:       dell
# Date:         2019/3/6
#-------------------------------------------------------------------------------
# def hello(type,*name):
#     sum = 0
#     if  type  == '*':
#         sum =ajaxdata.json
#         for item in name:
#             if type == '+':
#                 sum += item
#             if  type == '*':
#                 sum *= item
#     return sum
#
# if __name__ == '__main__':
#     ret = hello('*',ajaxdata.json,2,3,4,5,6,7)
#     print(ret)
def hello(*name,type = '+'):
    sum = 0
    if type == '*':
        sum = 1
    for item in name:
            if type == '+':
                sum += item
            elif type == '*':
                sum *= item
    return sum
if __name__ == '__main__':
    ret = hello(1,2,3,4,5,6,7,type='*')
    print(ret)

    ret2 = hello(1,2,3,4,5,6,7,type='+')
    print(ret2)