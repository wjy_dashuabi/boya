# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         7.6
# Description:
# Author:       dell
# Date:         2019/2/26
#-------------------------------------------------------------------------------
def add1 (x,y,z):
    return  x + y + z
print(add1(1,2,3))

list =  [2,3,4]
print( add1( *list))

dict = {'x':100 , 'y': 200,'z':12 }
print( add1(**dict))
def add2 (*numbers):
    reslut  = 0
    for number in numbers:
        reslut += number
    return reslut
print(add2(1,2,3,4,5))
print(add2(*list))
def  add3 (**numbers):
    result = 0
    for item in numbers.items():
        result += item[1]
    return result
print(add3(**dict))
def  add4 (numbers):
    result = 0
    for item in numbers.items():
        result += item[1]
    return result
print(add4(dict))
