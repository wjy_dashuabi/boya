# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         7.7
# Description:
# Author:       dell
# Date:         2019/2/26
##-------------------------------------------------------------------------------
# 作用域
# x =123
# def  fun1():
#     x= 30
#     print(x)
# fun1()
def jc(n):
    if n== 0 or n == 1:
        return 1
    else:
        return n * jc(n-1)
print(jc(10))
def fibonacci(n):
    if n==1 :
        return 0
    elif n ==2 :
        return 1
    else:
        return fibonacci( n - 1) +fibonacci( n - 2 )
print(fibonacci(10))