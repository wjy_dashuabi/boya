# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         随堂练习3
# Description:
# Author:       dell
# Date:         2019/3/6
#-------------------------------------------------------------------------------
def hello(name,age,addr='博雅未来'):#关键字参数必须写在未知参数的后面
    print('hello {name}'.format(name = name))
    print('我今年{age}'.format(age = age))
    print('我在{addr}上课'.format(addr = addr))
    pass
if  __name__ == '__main__':
    hello(name='yan',age= 40)
    hello(age= 40,name= 'yan')
    print('--------------')

    hello('yan',40)
    hello(40,'yan')
    print('--------------')

    hello(age= 40 ,name= 'yan',addr='博雅')
