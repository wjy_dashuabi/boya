# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         随堂练习2
# Description:
# Author:       dell
# Date:         2019/3/6
#-------------------------------------------------------------------------------
def hello(name):
    '问候函数'
    print('hello {}'.format(name))
    return
if  __name__  == '__main__':
    ret = hello('王某人')
    print(ret)
    print('__doc__方式',hello.__doc__)#返回函数的
    print('help方式',help(hello))