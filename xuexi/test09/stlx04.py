# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         stlx05
# Description:
# Author:       dell
# Date:         2019/3/20
#-------------------------------------------------------------------------------
import random

class StartMoblieException(Exception):
    pass

class Moblile():

    def start(self):

        num = random.randint(1, 10000)
        if num < 50:
            raise StartMoblieException("随机数小于50")
        elif num>=50 and num <=999:
            num = "{:0<4}".format(num)
        new_num = 1580733 + num
        print(new_num)

m = Moblile()
m.start()


