# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         stlx05
# Description:  else语句
# Author:       dell
# Date:         2019/3/23
#-------------------------------------------------------------------------------
a = 10
b = 0

# try:
#     c = a/b
# except Exception as e:
#     print(e)
# else:
#     # 抛出异常else块不会被运行
#     print(c)
# print('over')


def div(a,b):
    c = 0
    try:
        c = a/b
    except Exception as e:
        print(e)
        return None#此处的return是没有作用的
    finally:
        c =  c + 100
        return c

if  __name__ =='__main__':
    d= div(a,b)
    print(d)
