# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         suitangyanlian02
# Description:  自定义异常类
# Author:       dell
# Date:         2019/3/20
#-------------------------------------------------------------------------------
class Myexception(Exception):
    def __init__(self,*args,**kwargs):
        for item in args:
            print(item)

a = 10
b = 0
if b == 0 :
    raise Myexception('除数不能为0')
else:
    print(a/b)

print('------------------------------')
