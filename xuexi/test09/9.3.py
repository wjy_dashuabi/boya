# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         0.3
# Description:
# Author:       dell
# Date:         2019/3/2
#-------------------------------------------------------------------------------
class Xdd(Exception):
    pass
class Wjy(Exception):
    pass
class Lx:
    def add(self,x,y):
        if  x<0 or y<0:
            raise Xdd
        return x+y
    def sub(self,x,y):
        if  x - y <0 :
            raise Xdd
    def mul(self,x,y):
        if x==0 or y==0:
            raise Wjy
        return x*y
    def div(self,x,y):
        return x/y

while True:
    try:
        calc = Lx()
        expr = input('请输入要计算的表达式，例如，add(ajaxdata.json,2):')
        if expr == ':exit':
            break;
        result = eval('calc.'+expr)
        print("计算结果：{:2f}".format(result))
    except Xdd:
        print('*****负数异常*****')
    except Wjy:
        print('*****操作数为0异常')
    except ZeroDivisionError:
        print('****分母不能为零')
    except:
        print('***其他异常')