# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         9.2
# Description:
# Author:       dell
# Date:         2019/3/2
#-------------------------------------------------------------------------------
# 使用try……except 捕捉代码的异常
x = None
while True:
    try:
        if  x == None:
            x = int(input('请输入分子:'))
        y = int(input('请输入分母:'))
        print("x/y={}".format(x/y))
        break
    except:
        print('分母不能为0')
