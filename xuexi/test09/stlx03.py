# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         stlx04
# Description:
# Author:       dell
# Date:         2019/3/20
#-------------------------------------------------------------------------------
num = 100
a_list =[1,2,23,45,'p']

def chfa(*args):
    for item in args:
        print(num / item)

if  __name__ == '__main__':
    try:
        chfa(*a_list)
    except Exception as e :
        print(e)
    # 后面的语句可以执行
    print('列表为:',a_list)