# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         9.4
# Description:
# Author:       dell
# Date:         2019/3/2
#-------------------------------------------------------------------------------
class Wjy(Exception):
    pass
class  Wjy1(Exception):
    pass
class  Wjy2(Exception):
    pass
import  random
def raiseException():
    n = random.randint(1,3)
    print('抛出Wjy{}异常'.format(n))
    if n ==1 :
        raise Wjy
    elif n ==2 :
        raise Wjy1
    else:
        raise Wjy2
try:
    raiseException()
except(Wjy,Wjy1,Wjy2):
    print('执行异常处理程序')
