# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         9.5
# Description:
# Author:       dell
# Date:         2019/3/2
#-------------------------------------------------------------------------------
class Wjy(Exception):
    pass
class ZeroException(Exception):
    pass
class SprcialCalc:
    def add(self,x,y):
        if  x<0 or y<0:
            raise Wjy('X和Y都不能小于0哦')
        return x+y
    def sub(self,x,y):
        if x -y <0:
            raise Wjy('X和Y的差值不能小于0哦')
        return x-y
    def mul(self,x,y):
        if  x == 0 or y ==0:
            raise ZeroException('x和y都不能为0哟')
        return x * y
    def div(self,x,y):
        return x/y
while True:
    try:
        calc = SprcialCalc()
        expr = input('请输入要计算的表达式，例如，add(ajaxdata.json,2):')
        if expr == ':exit':
            break;
        result = eval('calc.' + expr)
        print("计算结果：{:2f}".format(result))
    except(Wjy,ZeroException)as e:
        print(e)
    except ZeroDivisionError as e:
        print(e)
    except:
        print("其他异常")
