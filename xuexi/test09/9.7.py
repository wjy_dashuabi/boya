# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         9.7
# Description:
# Author:       dell
# Date:         2019/3/2
#-------------------------------------------------------------------------------
# 异常捕捉中的finally语句
def fun1():
    try:
        print('fun1 正常执行')
    finally:
        print('fun1 finally')
def fun2():
    try:
        raise Exception#一旦执行了raise语句后面的语句将不会执行
    except:
        print('fun2 抛出异常')
    finally:
        print('fun2 finally')
def fun3():
    try:
        return 20
    finally:
        print('fun3 finally')
def fun4():
    try:
        x=1/0
    except ZeroDivisionError as e:
        print(e)
    finally:
        print('fun4 finally')
        del x
fun1()
fun2()
print(fun3())
fun4()

