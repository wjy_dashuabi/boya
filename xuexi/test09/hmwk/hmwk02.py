
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         hmwk01
# Description:
# Author:       dell
# Date:         2019/3/27
#-------------------------------------------------------------------------------
class JCException:
    pass


class JC:
        def compute(self, n):
            # 递归调用终止条件
            if n < 0:
                raise JCException('参数要求大于0')
            if n == 0 or n == 1:
                return 1
            else:
                return n * self.compute(n - 1)

if __name__ == '__main__':
    jc = JC()
    print(jc.compute(3))
    try:
        print(jc.compute(4))
    except JCException as e:
        print(e)

