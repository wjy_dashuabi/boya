
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         6.3
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/31
#-------------------------------------------------------------------------------
values1= (1,3,'hello')
str1 = 'adc %d,xyz %d,%s world'
print(str1 % values1)
values2 = {'title':'即可奇缘','url':'http://geekor.com','company':'欧瑞科技'}

str2  = """
<自学的乱七八糟>
  <head>
  <title> {title}</title>
  <mate charset = 'utf-8'/>
  <head>
  <body>
  <h1>{title}</h1>
  <a href = "{url}">{company}</a>
  </body>
</自学的乱七八糟>
"""
print(str2.format_map(values2))