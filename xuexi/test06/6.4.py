# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         6.4
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/31
#-------------------------------------------------------------------------------
# dict = {'x':ajaxdata.json , 'y':2 , "z":3}
# for key in dict:
#     print(key,)
# for key,values in dict.items():
#     print(key, values,)
# names = ['Bill','Mary','Jhon']
# ages = [30,40,20]
# for i in range(len(names)):
#     print(names[i],ages[i],end= ' ')
#
# companies = ['欧瑞科技','Google','Facebook']
# wbsites = ['222', '2222']
# for value in zip(companies,wbsites):
#     print(value,end=' ')
#
# companies = reversed(companies)
# for value in companies:
#     print(value)

dict = {'name':'Bill','age':34, 'sex': '男', 'salary':3000}
for key in dict :
    print(key,'=',dict[key],end = ' ')
print()

for key , value in dict.items():
    print(key, '=', value, end=' ')
print()

list1  = [1,2,3,4]
list2 = ['a','b','c','d']
for i  in  range(len(list1)):
    print('list1['+ str(i)  +']','=',list1[i],'list2[' + str(i) + ']','=',list2[i],end=' ')
print()

for value in zip(list1,list2):
    print(value,end='')
print()

valuesl = [4,1,3,6,5,2,8]
print(sorted(valuesl))
values2 = reversed(valuesl)
for v in values2:
    print(v,end=' ')
print()
print(''.join(list(reversed('hello world'))))