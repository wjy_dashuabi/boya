# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         6.5
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/31
#-------------------------------------------------------------------------------
names1 = {'Bill':20,'Mike':30,'John':40}
names2 = names1
print(names2)
names1 = {}
print(names2)
names1 = {'Bill':20,'Mike':30,'John':40}
names2 = names1
names1.clear()
print(names2)