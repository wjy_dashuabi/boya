# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         6.11
# Description:
# Author:       dell
# Date:         2019/2/14
#-------------------------------------------------------------------------------
dict = {}
print(dict.setdefault('name','Bill'))
print(dict)
print(dict.setdefault('name','Mike'))
print(dict)
print(dict.setdefault('age'))
print(dict)
