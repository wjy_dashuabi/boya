# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         随堂练习
# Description:
# Author:       dell
# Date:         2019/3/2
#-------------------------------------------------------------------------------
d = {}
d['name'] = 'by'
d['count']= 16
print(d)

d[12] = '100'
print(d)

d[0.1]  = 'aa'
print(d)

t=(10,20)
d[t] = 'tuple'
print(d)


print(len(d))
d['name'] = 'kkkk'

for item in d:
    print(item)
