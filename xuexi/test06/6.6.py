# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         6.6
# Description:
# Author:       dell
# Date:         2019/2/2
#-------------------------------------------------------------------------------
person1 =  {'Name':'Bill','age':30,'fullname':['Bill','Gates']}
person2 = person1.copy()
print('person1',person1)
print('person2',person2)
person2['age']= 54
print('person1',person1)
print('person2',person2)
person2['fullname'][1] = 'Clinton'
print('person1',person1)
print('person2',person2)
print('--------深层复制---------')
from copy import  deepcopy
person1 = {'name':'Bill','age':30,'fullname':['Bill','Gates']}
person2 = person1.copy()
person3 = deepcopy(person1)
person2 ['fullname'][1]='Clinton'
print('person1',person1)
print('person2',person2)
print('person3',person3)

#复制只复制了第一层的，所以复制的字典改变第一层的值对第一层不会有影响。而第二层的值，任何一个字典改变都会影响另一个字典