# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         6.7
# Description:
# Author:       dell
# Date:         2019/2/2
#-------------------------------------------------------------------------------
newDict1 = {}.fromkeys(['name','company','salary'])
print(newDict1)
newDict2 = newDict1.fromkeys(('name','company','salary'))
print(newDict2)
newDict3 = newDict1.fromkeys(['name','company','salary'],'没有值')
print(newDict3)