# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         6.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/31
#-------------------------------------------------------------------------------
names  = ['Bill','Mike','John']
numbers = ['123','456','789']
print(numbers[names.index('Bill')])

items =[['Bill','1234'],['Mike','4321'],['Mary','7753']]
d = dict(items)
print(d)

items =[]
while True:
    key = input('请输入key：')
    if key == 'end':
       break;
    values  = input('请输入values：')
    keyValues = [key,values]
    items.append(keyValues)
d = dict(items)
print(d)