# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         6.12
# Description:
# Author:       dell
# Date:         2019/2/14
#-------------------------------------------------------------------------------
dict1 ={
    'title' : '欧瑞学院',
    'website':'www.123.com',
    'description':'王景渊'
}
dict2 = {
    'title':'欧瑞学院',
    'website':'www.123.com ',
    'name' : 'wjy'
}
dict1.update(dict2)
for item in dict1.items():
    print('key={key},  value = {value}'.format(key = item[0], value = item[1]))