# # -*- coding: utf-8 -*-#
# #-------------------------------------------------------------------------------
# # 建立者:       王景渊
# # Name:         讲义
# # Description:
# # Author:       dell
# # Date:         2019/3/2
# #-------------------------------------------------------------------------------
# 字典

# ajaxdata.json 了解字典的概念
# 2 掌握如何用 dict 函数将序列转换为字典
# 3 掌握字典的基本操作
# 4 掌握如何用字典中的元素格式化字符串
# 5 掌握如何迭代字典和其它序列
# 6 掌握字典中常用的方法
#
# ajaxdata.json 为什么要引入字典
#     类似现实中的字典 中华字典 成语字典等
#     通过键值名称 获取元素值 简单明了
#
# 2 创建和使用字典
#     定义
#     dict_obj  = {'key1':value1,'key2':value2}
#     键值对的形式呈现
#     键与值用 : 分隔
#     每对键值间用 , 分隔
#
#     2.ajaxdata.json dict 函数
#         names = [
#             ['zhangsan':20],
#             ('lisi':30),
#             ['wangwu':50]
#         ]
#
#         dict_obj = dict(names)
#
#         dict() 不带参数返回空字典
#
#
#     2.2 字典的基本操作
#         len(dict_obj) 键值对的数量
#         dict_obj[key] 获取 key 上的值
#         dict_obj[key] = value 设置 key 上的值
#         key in dict_obj 检查 dict_obj 中是否包含有键为 key 项
#
#         字典与列表的区别
#             ajaxdata.json 键类型
#                 字典的 key 是任意不可变类型 浮点数 元组 字符串等
#                 列表的 key 只能为整型
#
#             2 自动添加
#                 字典可以用过键值自动添加新的项
#                 列表 必须 通过  append 或 insert 添加
#             3 key in dict_obj
#                 字典 : 查找的是 key
#                 列表 : 查找的是 value
#
#         字典 通过 key 查找效率更高
#
#     2.3 字典的格式化字符串
#         format_map 函数
#
#         eg :
#             str1 = '''
# #                 hello {name} 先生/女士
# #                 我是{work}
#             '''
#             str1.format_map({'name':'Yzl','work':'软件开发者'})
#
#     2.4 序列与迭代
#         序列,元组,字典都是可迭代的
#
#         2.4.ajaxdata.json 获取字典中key的列表
#
#             eg:
#                 dict = {'a':ajaxdata.json,'b':2}
#                 for key in dict:
#                     print(key)
#
#         2.4.2 同时获取字典中 key value 列表
#             items()函数
#
#             eg:
#                 for key,value in dict:
#                     print(key,value)
#
#         2.4.3 并行迭代
#             同时迭代多个列表,使用range()函数
#                 list1 = [11,22,33]
#                 list2 =['aa','bb','cc']
#                 length = len(list1)
#                 for i in range(length):
#                     print(list1[i],list2[i])
#
#         2.4.4 压缩列表
#             zip() 函数 将两个或多个序列的对应元素作为一个元组放到一起,以元素个数最少的为准
#             eg:
#                 for v in zip(list1,list2):
#                     print(v)
#
# 3 字典方法
#     3.ajaxdata.json clear()
#         除字典里所有 items 。不接受任何参数，返回值为 None
#         eg:
#             dict.clear()
#
#     3.2 copy() deepcopy()
#
#
#     3.3 fromkeys()
#         传入一个可迭代的对象，遍历分别作为键，建立新的字典，没个键对应的值都为 None 。
#         若不想以 None 作为默认值，也可自己提供默认值。
#         接受必备一个可迭代对象作为必备参数，默认参数可修改
#
#     3.4 get()
#         通过键名访问对应的值。
#         与常用的访问方法的区别是：若字典里没有所访问的键，则程序会报错，使得程序运行不下去；
#         get方法则会返回 None。还可以自己定义默认值，替换 None
#
#     3.5 items() keys()
#         items:
#         将字典所有的键值对以 dict_items 的类型返回，可以转换成列表，元组等。
#         列表中的每一项都表示为 （键，值） 的形式。
#         由于字典是无序的，故返回时并没有遵循特定的次序。不接受任何参数
#
#         keys
#         将字典里的所有键以 dict_keys 的类型返回。可以转换成列表，元组等。不接受任何的参数
#
#     3.6 pop() popitem()
#         pop:
#         删除指定键和其对应的值，并且返回指定键对应的值。
#         若指定键不存在，一种情况程序会报错，另一种情况可以指定一个值作为返回值
#
#         popitem:
#         与列表的 pop 方法类似，但与之不同的是，由于字典是无序的，故是随机以（键，值）的形式抛出，并且返回（键，值）。
#         不接受任何参数
#
#     3.7 setdefault()
#         与get方法类似。不同的是若字典里没有所访问的键，不仅会返回 None ，同时会在字典创建一个新的键值对，值默认为 None ，也可以设定默认值
#
#     3.8 update()
#         利用一个字典项更新另一个字典。
#         将一个字典里的项添加到另一个字典里，若两个字典里有相同的键，则更新其键对应的值
#
#     3.9 values()
#         将字典里的所有值以 dict_values 的类型返回，可以转换成列表，元组等。
#         不接受任何参数，用法和keys一样
#
#
#
