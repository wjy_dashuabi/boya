# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         14.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/4/17
#-------------------------------------------------------------------------------
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:        博智科技
# Name:         demo1411
# Description:
# Author:       yzl
# Date:         2019-02-09
#-------------------------------------------------------------------------------

from xml.etree.ElementTree import parse

doc = parse('files/products.xml')

for item in doc.iterfind('products.xml/product'):
    uuid = item.get('uuid')
    id = item.findtext('id')
    name = item.findtext('name')
    price = item.findtext('price')

    print('uuid=',uuid)
    print('id=',id)
    print('name=',name)
    print('price=',price)

    print('------------------')



