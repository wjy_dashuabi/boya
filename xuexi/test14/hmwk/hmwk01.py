# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         hmwk01
# Description:
# Author:       dell
# Date:         2019/4/18
#-------------------------------------------------------------------------------

# 作业:请解析如下 json 字符串为 字典
# {"code":ajaxdata.json,"data":[{"id":"2","thd_name":"开度","s_id":"40","duxsel":"N","orderno":"13","product":[{"id":"6","title":"[大度]16开","t_id":"2","orderno":"99"},{"id":"7","title":"[大度]32开","t_id":"2","orderno":"99"},{"id":"8","title":"[大度]8开","t_id":"2","orderno":"99"},{"id":"9","title":"[大度]4开","t_id":"2","orderno":"99"}]},{"id":"6","thd_name":"纸张","s_id":"40","duxsel":"N","orderno":"14","product":[{"id":"24","title":"双铜128克","t_id":"6","orderno":"99"},{"id":"25","title":"双铜157克","t_id":"6","orderno":"99"},{"id":"26","title":"双铜200克","t_id":"6","orderno":"99"}]},{"id":"5","thd_name":"印面","s_id":"40","duxsel":"N","orderno":"15","product":[{"id":"22","title":"双面","t_id":"5","orderno":"99"},{"id":"23","title":"单面","t_id":"5","orderno":"99"}]},{"id":"4","thd_name":"数量","s_id":"40","duxsel":"N","orderno":"16","product":[{"id":"16","title":"500份","t_id":"4","orderno":"99"},{"id":"17","title":"1000份","t_id":"4","orderno":"99"},{"id":"18","title":"2000份","t_id":"4","orderno":"99"},{"id":"19","title":"4000份","t_id":"4","orderno":"99"},{"id":"20","title":"9000份","t_id":"4","orderno":"99"},{"id":"21","title":"10000份","t_id":"4","orderno":"99"}]},{"id":"3","thd_name":"款数","s_id":"40","duxsel":"N","orderno":"17","product":[{"id":"10","title":"1款","t_id":"3","orderno":"99"},{"id":"11","title":"2款","t_id":"3","orderno":"99"},{"id":"12","title":"3款","t_id":"3","orderno":"99"},{"id":"13","title":"4款","t_id":"3","orderno":"99"},{"id":"14","title":"5款","t_id":"3","orderno":"99"},{"id":"15","title":"自定义_1U_款","t_id":"3","orderno":"99"}]},{"id":"ajaxdata.json","thd_name":"工艺","s_id":"40","duxsel":"Y","orderno":"18","product":[{"id":"ajaxdata.json","title":"双面亮膜","t_id":"ajaxdata.json","orderno":"99"},{"id":"2","title":"压痕","t_id":"ajaxdata.json","orderno":"99"},{"id":"3","title":"UV","t_id":"ajaxdata.json","orderno":"99"},{"id":"4","title":"烫金","t_id":"ajaxdata.json","orderno":"99"},{"id":"5","title":"压纹","t_id":"ajaxdata.json","orderno":"99"}]}],"msg":"ok"}
import json

f = open('./file/hmwk.json', 'r', encoding='utf-8')
jsonStr = f.read()
data_dict = json.loads(jsonStr)
# 将json字符串变成字典


datas = data_dict['data']
# 提取出字典中以"data"开头的字符串
with open('json_data.txt', 'w+', encoding='utf-8') as f:
    # 写文件
    for data in datas:
        a = 'ID:' + data['id'] + ' 名称:' + data['thd_name'] + '\n'
        # 提取字符串data中的信息
        f.write(a)

        datas_product = data['product']
        # 提取出字典中以"product"开头的字符串
        for data_product in datas_product:
            b = '张数：' + data_product['id'] + ' 属性：' + data_product['title'] + '\n'
            f.write(b)
        #     提取字符串中product的信息
        f.write('\n')
