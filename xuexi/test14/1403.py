# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         1403
# Description:
# Author:       dell
# Date:         2019/4/17
#-------------------------------------------------------------------------------
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:        博智科技
# Name:         demo1413
# Description:
# Author:       yzl
# Date:         2019-02-10
#-------------------------------------------------------------------------------
import xmltodict

f = open('files/products.xml','rt',encoding='utf-8')
xml = f.read()
d = xmltodict.parse(xml)
f.close()
print(d)

# 打印的美化格式
import pprint
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(d)

# d = dict(d)
# 取属性
print(dict(d['root'])['products']['product'][0]['@uuid'])
# 取值操作
print(d['root']['products']['product'][0]['@uuid'])


