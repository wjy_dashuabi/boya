# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         14.01
# Description:
# Author:       dell
# Date:         2019/4/17
#-------------------------------------------------------------------------------
from xml.etree.ElementTree import parse

# 读入xml文档
try:
    doc = parse('files/products.xml')    # 前面什么都不写表名平级目录之下，返回的是一个文档对象
    for item in doc.iterfind('products/product'):
        print(item)
        id = item.findtext('id')
        name = item.findtext('name')
        price = item.findtext('price')
        print('uuid','=',item.get('uuid'))
        print('id','=',id)
        print('name','=',name)
        print('price','=',price)
        print('----------')
except Exception as e:
    print(e)
# for item  in doc.iterfind('products/person'):
#     print(item.findtext('name'))

