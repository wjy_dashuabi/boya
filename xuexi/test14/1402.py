# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         1402
# Description:   字典转化为xml字符串
# Author:       dell
# Date:         2019/4/17
#-------------------------------------------------------------------------------
from xml.dom.minidom import parseString
import dicttoxml
import os

d = [20,'names',
    {'name':'Bill','age':30,'salary':2000},
    {'name':'王军','age':40,'salary':3000},
    {'name':'John','age':20,'salary':5000},
]
#将列表转化为 xml对象
bxml = dicttoxml.dicttoxml(d,custom_root='persons')
xml = bxml.decode('utf-8')
print(xml)
dom = parseString(xml)
prettyxml = dom.toprettyxml(indent='  ')
f = open('files/persons.xml','w',encoding='utf-8')
f.write(prettyxml)
f.close

