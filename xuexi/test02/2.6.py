# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         test2.6
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/20
#-------------------------------------------------------------------------------
x = 1234.56789
print(format(x,'0.2f'))
print(format(x,'>12.1f'))
print(format(x,'<12.3f'),20)
print(format(x,'0>12.1f'))
print(format(x,'0<12.1f'))
print(format(x,'^12.2f'),3)
print(format(x,','))
print(format(x,',.2f'))
print(format(x,'e'))
print(format(x,'0.2E'))


