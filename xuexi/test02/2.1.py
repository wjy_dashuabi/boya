# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         2.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/15
#-------------------------------------------------------------------------------
x = 5432.5346
print(format(x,'0.3f'))
print(format(x,'0>10.2f'))
print(format(x,'0>10.2f'))
print(format(x,'0>10,.2f'))
print(format(x,'0^10.2f'))