# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         13.02
# Description:
# Author:       dell
# Date:         2019/4/13
#-------------------------------------------------------------------------------
import sys
import os
import re
# 从标准输入读取全部数据
text = sys.stdin.read()
files = text.split(os.linesep)
for file in files:
    result = re.match('.*readme.*',file)
    if result != None:
        print(file)
