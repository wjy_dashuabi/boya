# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         13.03
# Description:
# Author:       dell
# Date:         2019/4/13
#-------------------------------------------------------------------------------
import os
f = open('url.txt','a+')
url = ''
#指针移动到开始位置
while True:
    #读一行数据
    url = f.readline()
    #将最后的行结束符去掉
    url = url.rstrip()
    if url == '':
        break
    else:
        print(url)

f.seek(0)
# print(f.readlines()) 指针到了最后一行 \r \n
# 写入单行数据
f.write('https://www.baidu.com' + os.linesep)
f.close()

f = open('url.txt','a+')
urllist = [
    'https://www.google.com' + os.linesep,
    'https://www.sina.com.cn' + os.linesep
]
f.writelines(urllist)
f.close()