# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         13.01
# Description:
# Author:       dell
# Date:         2019/4/13
#-------------------------------------------------------------------------------
f = open('./file/test1.txt','w')
print(f.write('I love '))
print(f.write('python'))
f.close()

f = open('./file/test1.txt','r')
print(f.read(13))#读到了13位，指针到了第13位，往后没有读的了，所以要重置指针
print(f.read(7))
f.seek(0)#重置文件指针 就可以冲开头开始读取文件
print(f.read(6))
f.close()

try:
    f = open('./file/test2.txt.txt','r+')
except Exception as e :
    print(e)
#文件不存在就创建此文件

f = open('./file/test2.txt','a+')
print(f.write('hello'))
f.close()

f = open('./file/test2.txt','a+')
print(f.read())
f.seek(0)
f.close()

try:
    f = open('./file/test2.txt','w+')#清空并写入新的内容
    print(f.read())
    f.write('How are you')
    f.seek(0)
    print(f.read())
finally:
    f.close()#关闭的作用是，ajaxdata.json.释放系统资源
                           #2.防治下次打不开（主要的作用）