# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         hmwk01
# Description:
# Author:       dell
# Date:         2019/4/17
#-------------------------------------------------------------------------------
'''
作业：
    p266 第二题，推荐使用 jieba 库处理
    ajaxdata.json 文件读操作 从网上随便 copy 一段文字（中英文都行）
    2 使用结巴分词
    3 把分词结果写入到一个新文件中，采用面向对象的方法
    4 使用面向对象的方法
'''
import jieba

# encoding=utf-8
import jieba

# seg_list = jieba.cut("我来到北京清华大学", cut_all=True)
# # print("Full Mode: " + "/ ".join(seg_list))  # 全模式
# #
# # seg_list = jieba.cut("我来到北京清华大学", cut_all=False)
# # print("Default Mode: " + "/ ".join(seg_list))  # 精确模式
# #
# # seg_list = jieba.cut("他来到了网易杭研大厦")  # 默认是精确模式
# # print(", ".join(seg_list))
# #
# # seg_list = jieba.cut_for_search("小明硕士毕业于中国科学院计算所，后在日本京都大学深造")  # 搜索引擎模式
# # print(", ".join(seg_list))


word = '16日下午圆明园发声表态：衷心祈愿文物都能够远离灾难，代代传承。Old Summer Palace of China mourned Notre Dame on social media,'
# print(','.join(jieba.cut(word,cut_all= True)))
class Hmwk:
    # 进行分词
    def fenci(self):
        self.seglist = jieba.lcut(word,cut_all= True)


    # 添加文件并准备写入
    def cjwj(self):
        self.f = open('./files/hmwk01.txt','w+',encoding='UTF-8')

    # 写入字符串
    def write(self):
        self.word = self.f.write(' '.join(self.seglist))
        return self.word

if __name__ == '__main__':
    hmwk = Hmwk()
    hmwk.fenci()
    hmwk.cjwj()
    hmwk.write()





# try:
#     f = open('./file/test2.txt', 'w+')  # 清空并写入新的内容
#     print(f.read())
#     f.write('How are you')
#     f.seek(0)
#     print(f.read())
