# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         5.4
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/29
#-------------------------------------------------------------------------------
from string import Template
template1 = Template('$s是我最喜欢的编程语言，$s很容易学习，而且功能强大')
print((template1.substitute(s='python')))
template2 = Template('${s}stitute')
print(template2.substitute(s='sub'))
template3 = Template('$dollar$$相当于多少$pounds')
print(template3.substitute(dollar= 20 ,pounds ='英镑'))
template4 = Template('$dollar$$相当于多少$pounds')
data ={}
data['dollar']= 100
data['pounds'] = '英镑'
print(template4.substitute(data))



