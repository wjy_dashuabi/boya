# # -*- coding: utf-8 -*-#
# #-------------------------------------------------------------------------------
# # 建立者:       王景渊
# # Name:         讲义
# # Description:
# # Author:       dell
# # Date:         2019/3/2
# #-------------------------------------------------------------------------------
# 字符串
#
# ajaxdata.json 了解字符串的基本操作
# 2 掌握字符串格式化的基本方法
# 3 掌握模板字符串
# 4 掌握更高级的字符串格式化方法
# 5 掌握字符串的常用方法(函数)
#
# ajaxdata.json 字符串的基本操作
#     所有标准的序列操作(索引,分片,乘法,判断是否包含,长度,最大值,最小值)对字符串同样适应
#
# 2 格式化字符串
#     2.ajaxdata.json 字符串格式化基础(老方法,效率低)
#         相当于字符串模板
#         eg:
#         str = 'hello,%s'
#         print(str % '言先生')
#
#         %d,%f等
#
#         注意:指定的值不匹配,抛出异常
#
#     2.2 模板字符串
#         Template类的格式化参数用 $ 开头,后面跟着格式化参数名称
#         使用 substitute 方法,指定格式化参数对应的值
#
#         eg:
#             from string import Template
#             template = Template('$s是我最擅长的语言')
#             template.substitute(s='Python,php,lua')
#
#         当格式化参数是一个字符串的一部分时,需要用 {} 将格式化参数变量括起来
#
#     2.3 字符串的format 方法 (效率高,灵活,重点掌握)
#         str.format()，它增强了字符串格式化的功能
#         基本语法是通过 {} 和 : 来代替以前的 %
#         eg
#             print("网站名：{name}, 地址 {url}".format(name="博智科技", url="www.zzbozhi.net"))
#
#             # 通过字典设置参数
#             site = {"name": "博智科技", "url": "www.zzbozhi.net"}
#             print("网站名：{name}, 地址 {url}".format(**site))
#
#             # 通过列表索引设置参数
#             my_list = ['博智科技', 'www.zzbozhi.net']
#             print("网站名：{0[0]}, 地址 {0[ajaxdata.json]}".format(my_list))
#
#             #通过位置
#             print '{0},{ajaxdata.json}'.format('chuhao',20)
#
#     2.4 更进一步控制字符串格式化参数 p103 表5-ajaxdata.json
#         加入格式化类型符
#
#         eg:
#             #填充与对齐
#             print('{:>8}'.format('189'))
#
#             print('{:0>8}'.format('189'))
#
#     2.5 字段宽度,精度和千分分隔符
#
#         eg:
#             # 宽度
#             print("{num:12}".format(num='aa'))
#             print("{name:12}".format(name='yanzhaoliang'))
#
#
#             #保留两位小数
#             print('{:.2f}'.format(321.33345))
#
#             #用来做金额的千位分隔符
#             print('{:,}'.format(1234567890))
#
#     2.6 符号 对齐 用 0 填充和进制转换
#         < 左对齐
#         ^ 居中
#         > 右对齐
#
#         eg
#             print("{num:_>30}".format(num='yanzhaoliang'))
#
#
#         #其他类型 主要就是进制了，b、d、o、x分别是二进制、十进制、八进制、十六进制。
#             print('{:b}'.format(18)) #二进制 10010
#             print('{:d}'.format(18)) #十进制 18
#             print('{:o}'.format(18)) #八进制 22
#             print('{:x}'.format(18)) #十六进制 12
#
# 3 字符串方法
#     3.ajaxdata.json center
#         返回一个居中对齐的字符串，并使用指定长度的填充符号，不指定填充符号默认使用空格作为填充符号。
#         若指定的填充符号的长度小于字符串本身的长度，填充符号则不会显示
#
#         格式：“字符串”.center（填充的长度，“指定填充符号”）
#
#         eg:
#             s = 'python3'
#             s1 = s.center(30)
#             print('开始->',s1,'<-结束')
#
#     3.2 find (重要)
#         查找并返回指定str的索引位置，如果没找到则返回：-ajaxdata.json（查找的顺序是从左至右）
#         可以指定范围：开始位置和结束位置进行查找
#
#         格式："字符串内容".find('指定的字符串'，指定范围)
#
#         eg:
#             s = 'python'
#             s1 = s.find('h')
#             print(s1)
#
#     3.3 join (重要 软件开发)
#         定义：将序列中的元素以指定的字符拼接生成一个新的字符串，并返回这个字符串
#
#         格式：“指定字符串内容”.join（一个序列）
#
#         eg:
#             s = ''
#             t = ('k','n','i','g','h','t')
#             s1 = s.join(t) # 将序列中的元素以空字符串拼接一个成一个新的字符串
#             print(s1)
#
#     3.4 split(重要)
#         通过指定分隔符按从左到右的顺序对字符串进行切片，并以一个列表的形式返回。
#         括号内不指定字符串表示默认以空格为分隔符。可以指定分隔符的个数
#
#         格式：“字符串内容”.split（“指定分隔符”）
#
#         eg:
#             s = 'python big data'
#             s1 = s.split() # 以空格作为分隔符进行切片
#             print(s1)
#
#     3.5 lower
#         将字符串中的大写字母转换成小写字母，并返回转换成小写字母后的字符串。
#
#         格式："大写字母的字符串".lower（）
#
#         eg :
#             s = 'Dark Knight'
#             s1 = s.lower()
#             print(s1)
#
#     3.6 upper
#         定义：将字符串中的小写字母转换成大写字母，并返回转换成大写字母后的字符串
#
#         格式："小写字母的字符串".upper（）　
#
#         eg :
#             s = 'dark knight'
#             s1 = s.upper()
#             print(s1)
#
#     3.7 capwords
#         首字母大写
#         eg :
#             s = 'dark knight'
#             s1 = s.capwords()
#             print(s1)
#
#     3.8 strip(重要)
#         移除字符串左右两侧指定的字符，并返回移除指定字符串后的字符串，括号内不指定字符串默认移除空格。
#
#         格式："字符串内容".strip(‘指定字符串’)
#
#         eg:
#             s = '      dark knight    '
#             s1 = s.strip() # 移除字符串左右两侧的空格
#             print(s1)
#
#     3.9 replace
#         将指定的字符串替换成新的字符串，并返回替换后的字符串。
#         可以指定替换的次数，若不指定次数则默认替换全部。
#
#         格式：“字符串内容”.replace（“要替换的内容”，“新的内容”,指定的替换的次数）
#
#         eg:
#             s = 'dark knight dark sky'
#             s1 = s.replace('dark','暗黑')
#             print(s1)
#
#     3.10 translate mketrans (重要)
#         根据参数table给出的表(包含 256 个字符)转换字符串的字符,
#         要过滤掉的字符放到 deletechars 参数中。返回这替换后的字符串
#
#         格式：“原字符串”.translate（｛映射表｝）
#
#         原型格式
#         str.translate(table)
#         bytes.translate(table[, delete])
#         bytearray.translate(table[, delete])
#
#         返回值
#             返回翻译后的字符串,若给出了 delete 参数，则将原来的bytes中的属于delete的字符删除，
#             剩下的字符要按照table中给出的映射来进行映射
#
#         eg:
#             s1 = 'abcde'  # 原字符串中要替换的字符
#             num = '12345' # 相应的映射字符的字符串。
#             s2 = "aaxxbbxxccxxddxxee" # 原字符串
#
#             form = s2.maketrans(s1,num) # 创建映射表，让s1字符串中的字母分别对应num中的字母并指向s2字符串。
#             print(s2.translate(form)) # 使用上面映射表进行翻译，并把并把deletechars中有的字符删掉。
#
#
#             intab = "aeiou"
#             outtab = "12345"
#             trantab = str.maketrans(intab, outtab)   # 制作翻译表
#
#             str = "this is string example....wow!!!"
#             print (str.translate(trantab))
#
#
#             # 制作翻译表
#             bytes_tabtrans = bytes.maketrans(b'abcdefghijklmnopqrstuvwxyz', b'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
#
#             # 转换为大写，并删除字母o
#             print(b'runoob'.translate(bytes_tabtrans, b'o'))
#
#
#
