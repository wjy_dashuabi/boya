# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         5.11
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/30
#-------------------------------------------------------------------------------
list = ['a','b','c','d','e']
s = '+'
print(s.join(list))
dirs = '','user','local','nginx',''
linuxpath = '/'.join(dirs)
print(linuxpath)
windowpath = 'C:' + '\\'.join(dirs)
print(windowpath)
#使用join时 与字符串链接的序列元素必须是字符串类型，如果是其他类型数据类型，如数值，在调用join方法是报错
num = [1,2,3,4,5,6]
print(s.join(num))

