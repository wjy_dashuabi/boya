# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         5.10
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/30
#-------------------------------------------------------------------------------
s = 'hello world'
print(s.find('world'))
print(s.find('ok'))

print(s.find('o'))
print(s.find('o',5))
print(s.find('l',5,9))
print(s.find('l',5,10))

s= input( '请输入一个大字符串')
while True :
    subString = input('请输入一个字符串：')
    if   subString == 'end' :
        break
    startstr = input('请输入开始索引：')
    endstr = input('请输入结束索引：')
    start = 0
    end = len(s)
    if startstr !='':
        start = int(startstr)
    if endstr != '':
        end =int(endstr)
    print("'{}'在'{}'的出现位置是{}：".format(subString,s,s.find(subString,start,end)))