# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊
# Name:         5.3
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/29
#-------------------------------------------------------------------------------
formatstr1 ="PI是圆周率，他的值是%.4f（保留小数点后%d位）"
from math import pi
values1  = (pi, 4)
print(formatstr1 % values1)
formatstr2 = "这件事的成功率为%d%%,如果有%s参加的话，成功率提升至%d%%"
values2 = (56,'mike',78)
print(formatstr2 % values2)
values3 = (66,'mike')#此时由于指定的参数值的数量和格式化参数的数量不匹配，所以抛出异常
print(formatstr2 % values3)