# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         5.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/29
#-------------------------------------------------------------------------------
s1 = 'hello world'
print(s1[0])
print(s1[2])
print(s1[6:9])
print(s1[6:])
print(s1[::2])
s2 ='abc'
print(10*s2)
print(s2*5)
print('b' in s2)
print('x' not in s2)
print(len(s1))
print(min(s2))
print(max(s2))