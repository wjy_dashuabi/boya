# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊
# Name:         随堂练习
# Description:
# Author:       dell
# Date:         2019/2/27
#-------------------------------------------------------------------------------
s = 'python'
#center
s1 = s.center(30)
print('开始->',s1,'<-结束')
# find
s = 'python'
s1 = s.find('h')
print( s1)

# join
s = ''
t  =('k','n','i','g','h')
s1 = s.join(t)
print(s1)

s.split()
#要掌握
#strip
s = '         dark knight     '
s1 = s.strip()
print(s1)
#replace
s ='dark knight'
s1 = s.replace('dark','黑暗')
print(s1)

# 重要
# s.translate()
# s.maketrans()


s1 = 'hello world'
print(s1.find('world'))