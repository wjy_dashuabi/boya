
# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         5.6
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/30
#-------------------------------------------------------------------------------
print('原样输入：{first!s} 调用 repr 函数 ：{first!r} 输出函数Unicode 编码： {first!a}'.format(first = '中'))
print('{first!s}'.format(first = '中'))
print('{first!r}'.format(first = '中'))
print('{first!a}'.format(first = '中'))
print('整数:{num} 浮点数:{num:f}'.format(num=21))
print('十进制:{num} 二进制:{num:b} 八进制{num:o} 十六进制{num:x}'.format(num=56))
print('科学计数法：{num:e}'.format (num = 533))
print('百分比: {num:%}'.format(num = 0.56))