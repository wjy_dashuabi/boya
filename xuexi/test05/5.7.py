# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         5.7
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/30
#-------------------------------------------------------------------------------
print('{num:12}'.format(num = 52))
print('{name:10}Gates'.format(name = 'Bill'))
from math import pi
print('float number:{pi:.2f}'.format(pi=pi))
print('float number:{pi:10.2f}'.format(pi = pi))
print('{:.5}'.format('Hello World'))
print('One googol is {:,}'.format(10 ** 100))