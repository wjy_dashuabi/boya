# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         5.5
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/29
#-------------------------------------------------------------------------------
s1 = "Today is {},the temperature is {} degrees."
print(s1.format('Saurday',24))
s2  ='today is {week},the temperature is {degree} degree'
print(s2.format(degree = 22 , week = 'sunday'))

s3 = 'today is {week},{}, the {} temperature is {degree} degree'
print(s3.format('aaa',12345,week = 'sunday',degree  = 22))

s4 = 'today is {week},{ajaxdata.json} ,the {0} temperature is {degree} degree'
print(s4.format('aaa',12345,week = 'sunday',degree  = 22))

fullname = ['Bill','Gates']
print('Mr {name[ajaxdata.json]}'.format(name=fullname))

import math
s5 = 'The (mod._name_) module defines the values {mod.pi} for PI'
print(s5.format(mod= math))
