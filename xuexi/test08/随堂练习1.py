# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         随堂练习1
# Description:
# Author:       dell
# Date:         2019/3/13
#-------------------------------------------------------------------------------
class man(object):
    def helo(self):  # 第一个参数必须是self
        print('hello .......')
    def __init__(self):
        self.name = 'wjy'
        self.age = 40


if __name__ == '__main__':
    m = man()
    m.helo()  # 通过点来调用属性或方法
    print(m.age,m.name)
    print(type(m.age))

class Dog:
    pass

dog = Dog()
dog.name = 'wangwang'
print(dog.name)