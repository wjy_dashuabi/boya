# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         8.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/2/26
#-------------------------------------------------------------------------------
# 创建一个Person的类
class Person:
 def setName(self,name):
    self.name = name
 def getName(self):
    return self.name
 def greet(self):
    print('hello,I am {name}'.format(name = self.name))
person1 = Person()
person2 = Person()
person1.setName('Bill Gates')
person2.name ='Bill Clinton'
print(person1.getName())
person1.greet()
print(person2.name)
Person.greet(person2)


