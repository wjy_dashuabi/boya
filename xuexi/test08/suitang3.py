# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         suitang3
# Description:  类的继承
# Author:       dell
# Date:         2019/3/16
#-------------------------------------------------------------------------------
class Animal:
    def __init__(self):
        self.name = "Animal"
        print('Animal类')
    def eat(self):
        print('我在吃东西')

class Dog(Animal):
    def  __init__(self):
        self.name = 'dg'
        print('Dog类')
        print(self.name)
        super().__init__(       )
        print(self.name + 'Dog')

    def  walk(self):
        print('ajaxdata.json')
    def  eat(self):
#       super().eat() #调用父类的方法
        super(Dog,self).eat()
        print('狗爱吃骨头')
if __name__ =='__main__':

    animal = Animal()
    animal.eat()

    dog = Dog()
    dog.eat()
    dog.walk()


class Man:
    # pass
    def helo(self):
        return 'helo helo'

def hello():
    return 'hello'

if __name__ == '__main__':
    m = Man()
    m.name = 'aaa'
    print(hasattr(m,'name'))

    print('---------------')
    age = getattr(m,'age',40)
    print(age)

    print('---------------')
    #检测对象是否有 helo 这个函数，如果没有就返回，hello
    helo1 = getattr(m, 'helo' , hello)
    print(helo1())

    print(type(setattr(m,'height',170)))
    print(m.height)


    setattr(m,'name','bbb')
    print(m.name)
