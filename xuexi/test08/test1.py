# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         test1
# Description:
# Author:       dell
# Date:         2019/3/13
#-------------------------------------------------------------------------------
# class Student:
#     def __init__(self):
#         self.name = input('请输入姓名：')
#         self.age = input('请输入年龄：')
#         self.zhuanye = input('请输入专业：')
#         self.xi = input('请输入院系：')
#         self.xm = input('请输入校名：')
#
#     def getinfo(self):
#         return 'My name is {3},I am {4},I study at {0}{ajaxdata.json}{2}'.format(self.xm, self.xi, self.zhuanye, self.name,self.age )
#
# if __name__ == '__main__':
#     s = Student()
#     print((s.getinfo ()))
class Student:
    def info(self,name,age,major,yuan,xiaoming):
        self.name = name
        self.age = age
        self.major = major
        self.yuan = yuan
        self.xiaoming = xiaoming
    def getinfo(self):
        return "我的名字叫{3},今年{4},就读于{0}{1}{2}".format(self.name,
                                                         self.age,
                                                         self.major,
                                                         self.yuan,
                                                         self.xiaoming)
if  __name__ =='__main__':
    s = Student()
    s.info('wjy','18','信管','商院','学校名称')
    print(s.getinfo())
