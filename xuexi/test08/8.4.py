# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         8.4
# Description:
# Author:       dell
# Date:         2019/2/28
#-------------------------------------------------------------------------------
# 父类
class ParentClass:
    def method1(self):
      print('method1')
class ChildClass(ParentClass):
    def method2(self):
        print('method2')
child = ChildClass()
child.method1()
child.method2()