# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         9.2
# Description:
# Author:       dell
# Date:         2019/2/26
#-------------------------------------------------------------------------------
class Myclass:
    def getName(self):
        return self.name
    def setName(self,name):
        self.name  = name
        self.__outName()
    def __outName(self):
        print('Name={}'.format(self.name))
myClass = Myclass()
import inspect
methods = inspect.getmembers(myClass,predicate=inspect.ismethod)
print(methods)
for method in methods:
    print(method[0])
print('----------')
myClass.setName('Bill')

print(myClass.getName())
myClass._Myclass__outName()
print(myClass.__outName())