# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊
# Name:         hmwk01
# Description:
# Author:       dell
# Date:         2019/3/13
#-------------------------------------------------------------------------------
# 每个类至少三个属性三个方法  年龄姓名性别必须有
# 方法必须要有注释 python的标准方式
# 写一个测试程序验证
class Person:

    def __init__(self,name,age,sex,greet):
        '构造方法，获取姓名，性别，年龄'
        self.name = name
        self.age = age
        self.sex = sex
        self.greet = greet
    def method_greet(self):
        '问候语'
        if self.greet:
            print('老师好')
        else:
            print('同学们好')
        def result_out():
            '输出信息'
            print('我的名字是{}，我今年{}岁，我的性别是{}'.format(self.name,self.age,self.sex))
        return result_out()

class Student(Person):
    def teacher(self,teacher):
        '老师姓名'
        self.teacher = teacher
    def get_teache(self):
        '输出老师姓名'
        print('my teacher is {}'.format(self.teacher))
        def greet():
            '问候老师'
            return 'Good Morning,{}}'.format(self.teacher)
        return greet()

class study(Person):
    def subject(self, subject):
        '获取学科'
        self.subject = subject

    def get_subject(self):
        '输出喜欢的学科'
        print('I like study {}'.format(self.subject))

    def friend(self, friend):
        '输出最好的朋友'
        self.friend = friend
        print('my best friend is {}'.format(self.friend))

if __name__ == '__main__':
    s = study('wjy','20','男',True)
    s.method_greet()
    s.subject('English')
    s.get_subject()
    s.friend('John')

# 不会闭包，问了别人的