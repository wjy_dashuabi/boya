# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         hmwk02
# Description:
# Author:       dell
# Date:         2019/3/16
#-------------------------------------------------------------------------------
# 作业 三个类，有一个是基类people，老师，学生继承自人，老师去教工食堂吃饭，住在家里或者教师公寓，学生住去普通食堂吃饭，
#        住在学生宿舍，老师讲课，学生上课，共有的属性，姓名，年龄，性别，身高，体重，定义出共有方法和特有方法各三个，特有属性
#        定义一个以上，
class People:
    # 创建类
    def __init__(self,name,age,sex):
        # 构造方法，获取姓名，性别，年龄
        self.name =name
        self.age = age
        self.sex = sex
    def wjy(self):
        return '我们都需要吃饭和休息'
    def lx(self):
        return '我们都要呼吸空气'
    def boya(self):
        return '我们都是人'
#     定义三个共有方法

class Teacher(People):
    # 创建教师类，继承People的属性
    def stay(self):
        return '我住在教师公寓'
    def eat(self):
        return '我去教工食堂吃好吃的'
    def money(self):
        return '我每个月有9000元工资'
    def zhicheng(self):
        pass
    def shuchu(self):
        return '我叫{},我今年{},我是个{},{}{}并且{},我还是个python的{},{},{}!{}'.format(t.name, t.age,
                                                                        t.sex, t.stay(),
                                                                        t.eat(), t.money(),
                                                                        t.zhicheng, t.wjy(),
                                                                        t.boya(), t.lx())


class Student(People):
    # 创建学生类，继承People的属性
    def stay(self):
        return '我住在学生宿舍'
    def eat(self):
        return '我在普通食堂吃饭'
    def xuefei(self):
        return '我每年要交10000元学费'
    def shuru1(self):
        return '我叫{},我今年{},我是个{},{}{}并且{}，{},{},{}'.format(s.name, s.age,
                                                       s.sex, s.stay(),
                                                       s.eat(), s.xuefei(),
                                                       s.wjy(), s.boya(),
                                                       s.lx())


if  __name__ == '__main__':
    s = Student('wjy','18','man')
    # 输出叫wjy 的学生的信息
    print(s.shuru1())
    t = Teacher('yangong','30','man')
    # 输出叫yangong 的老师的信息
    t.zhicheng = '大牛'
    # 定义老师的职称
    print(t.shuchu())