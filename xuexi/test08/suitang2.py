# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         suitang2
# Description:
# Author:       dell
# Date:         2019/3/16
#-------------------------------------------------------------------------------
# class Animal:
#     def __init__(self):
#         print('这是构造函数')
#     # 构造函数
#     def __init__(self,age):
#         print(age)
#     def doprint(self,msg):
#         print(msg)
#
# if  __name__ == '__main__':
#     animal = Animal(30)
#     animal.doprint('hello')

# 传多个参数进去，初始化属性，例如传列表，字典
#z 同时输出
class Work:
   def __init__(self,*age,**wjy):

        self.age = age
        self.wjy = wjy


if __name__ == '__main__':
    qwe = Work()
    qwe.__init__(*[1,2,3],**{'age':10})
    print(qwe.age,qwe.wjy)



