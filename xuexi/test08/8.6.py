# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         8.6
# Description:
# Author:       dell
# Date:         2019/2/28
#-------------------------------------------------------------------------------
# 多继承
class Calculator:
    def calculate(self,expression):
        self.value = eval(expression)
    def printResult(self):
        print('result:{}'.format(self.value))
class MyPrint:
    def printResult(self):
        print('计算结果:{}'.format(self.value))
class NewCalculator(Calculator,MyPrint):
    pass
class NewCalculator1(MyPrint,Calculator):
    pass
calc = NewCalculator()
calc.calculate("ajaxdata.json+3*5")
calc.printResult()
print(NewCalculator.__bases__)
calc1 = NewCalculator1()
print(NewCalculator1.__bases__)
calc1.calculate('ajaxdata.json+3*5')
calc1.printResult()