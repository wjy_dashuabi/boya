# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         8.3
# Description:
# Author:       dell
# Date:         2019/2/28
#-------------------------------------------------------------------------------
# 类代码块
class MyClass:
    print('MyClass')
    count = 0
    def counter(self):
        self.count +=1
my = MyClass()
my.counter()
print(my.count)
my.count ='abc'
print(my.count)
my.name = 'hello'
print(my.name)
