# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         8.5
# Description:
# Author:       dell
# Date:         2019/2/28
#-------------------------------------------------------------------------------
# 判断类与类之间的关系可以使用issubclass函数
class MyParentClass:
    def mrthod(self):
     return 50
class ParentClass(MyParentClass):
    def method1(self):
      print('method1')
class MyClass:
    def method(self):
     return 40
class ChildClass(ParentClass):
    def method2(self):
        print('method2')
print(issubclass(ChildClass,ParentClass))
print(issubclass(ChildClass,MyClass))
print(issubclass(ChildClass,MyClass))
print(ChildClass.__bases__)
print(ParentClass.__bases__)
child = ChildClass()
print(isinstance(child,ChildClass))
print(isinstance(child,ParentClass))
print(isinstance(child,MyParentClass))
print(isinstance(child,MyClass))