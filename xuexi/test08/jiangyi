类和对象

1 了解什么是对象和类
2 了解类的三个主要特征:继承,封装和多态
3 掌握创建类的方法
4 掌握如何为类添加私有方法
5 掌握如何继承一个或多个类(多继承)
6 掌握如何检测类之间的继承关系

1 对象的魔法
    对象(object)可以看做数据以及可以操作这些数据的一系列方法的集合
    1.1 继承(inheritance):当前类从其他类获得资源(数据和方法),以便更好地代码重用,并且可以描述类与类之间的关系
    1.2 封装(encapsulation):对外部世界隐藏对象的工作细节
    1.3 多态(polymophism):多态意味着同一一个对象同样的操作在不同的场景会有不同的行为（python没有明确的多态）


2 类
    2.1 创建自己的类
        class 关键字定义
        eg :
            class Person(object):#object可以省略
                def fn1(self):
                    statement
                def fn2(self):
                    statement


        说明:
            1 类也是一个代码块（类首字母大写，方法首字母小写）
            2 类中的方法其实就是函数
            3 每一个方法的第一个参数必须是 self (可以是其它名字,指向自己)
            4 通过 self 参数添加类的属性,可以在外面访问  
            5 类创建对象与使用函数的方式相同
            6 调用对象的方法有两种方式:
                1 直接通过对象变量调用方法
                2 直接通过类调用方法

    2.2 方法和私有化
        默认情况下方法都可以被外部访问
        方法名前面加双下划线 __ 可以让该方法在外部不能访问
        不是绝对的,通过如下方式可以被外部访问
            obj._Person__method2()

    2.3 类代码块
        在class 代码块中可以包含任何语句
        可以动态向class代码块中添加新成员

    2.4 类的继承
        继承:是指一个类(子类)从另外一个类(父类)中获得所有成员
        python支持多继承,建议尽可能的少用多继承

        eg :
            class Parentclass:
                def fn1(self):
                    statement

            class Boyclass(Parentclass):
                def fn2(self):
                    statement

    2.5 检测继承关系
        issubclass('子类','父类') 返回值 True/False
        demo825.py

    2.7 接口
        python中没有接口
        对象调用一个方法是假设存在这个方法存在

        hasattr(obj,'方法名/属性')  返回值 True/False

        getattr(obj,'方法名/属性','默认方法名/属性') 返回值 存在 方法名 否则 默认方法名

        有则更新,没有则添加
        setattr(obj,'方法/属性','设置的方法名/属性')

       作业 三个类，有一个是基类people，老师，学生继承自人，老师去教工食堂吃饭，住在家里或者教师公寓，学生住去普通食堂吃饭，
       住在学生宿舍，老师讲课，学生上课，共有的属性，姓名，年龄，性别，身高，体重，定义出共有方法和特有方法各三个，特有属性
       定义一个以上，


3 魔术方法
    3.1 基本魔术方法
        __new__(cls,[...])
            1 __new__ 是在对象实例化的时候所调用的第一个方法
            2 第一个参数是这个类,其它参数是用来直接传递给 __init__方法
            3 __new__ 决定是否要使用该 __init__ 方法,因为 __new__ 可以调用其它类的构造方法或者直接返回别的实例对象
                作为本类的实例,
                如果 __new__ 没有返回实例对象,则__init__不会被调用
            4 __new__ 主要用于继承一个不可变的类型比如 int,tuple,string

            5 __new__ 用来实例单例

            demo0831.py

            https://blog.csdn.net/m0_37519490/article/details/80713065

        __init__(self,[...])
            构造器

        __del__(self,[...])
            析构器

        __call__(self,[...])
            允许一个类的实例函数一样被调用
            eg: x(a,b) 调用 x.__call__(a,b)

        __len__(self)
            定义当被len()调用行为

        __repr__(self)
            定义被repr()调用行为

        __str__(self)
            定义被str()调用行为

        __bites__(self)
            定义被bytes()调用行为

        __hash__(self)
            定义被hash()调用行为

        __bool__(self)
            定义被bool()调用行为

        __format__(self,format_spec)
            定义被format()调用行为

    3.2 有关属性
        __getattr__(self,name)
            定义当用户试图获取一个不存在的属性的行为

        __getattrbute__(self,name)
            定义当该类的属性被访问时的行为

        __setattr__(self,name)
            定义当一个属性被设置时的行为

        __delattr__(self,name)
            定义当一个属性被删除时的行为

        __dir__(self)
            定义当dir()被调用时的行为

        __get__(self,instance,owner)
            定义当描述符的值被取得时的行为

        __set__(self,instance,value)
            定义当描述符的值被改变时的行为

        __delete__(self,instance)
            定义当描述符的值被删除时的行为

    3.3 比较操作符
        __lt__(self,other)
            定义小于号行为 : x < y 调用 x.__lt__(y)

        __le__(self,other)
            定义小于等于号行为 x <= y 调用 x.__le__(y)

        __eq__(self,other)
            定义等于号行为   x == y 调用 x.__eq__(y)

        __gt__(self,other)
            定义大于号行为 : x > y 调用 x.__gt__(y)

        __ge__(self,other)
            定义大于等于号行为 : x >= y 调用 x.__ge__(y)




    参考资料
    https://blog.csdn.net/Ka_Ka314/article/details/80402143


4 装饰器

    参考资料
    https://www.cnblogs.com/ArmoredTitan/p/8550892.html
