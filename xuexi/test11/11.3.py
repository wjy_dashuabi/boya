# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         11.3
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 匹配多个字符串:使用在择一匹配符（|）,和逻辑或类似，只要满足任何一个，就算匹配成功
import re
s ='Bill|Mike|Jhon'
m = re.match(s,'Bill')
if  m is not  None:
    print(m.group())
m = re.match(s,'Bill: my friend')
if   m is not None:
    print(m.group())
m = re.match(s,'My friend is Bill')
if m is not None:
    print(m.group())
print(m)#同理11.2

m = re.search(s,'where is Mike')
if m is not None:
    print(m.group())
print(m)
