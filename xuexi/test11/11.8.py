# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         11.8
# Description:
# Author:       dell
# Date:         2019/3/7
#-------------------------------------------------------------------------------
import re
m = re.search('^The','The end')
print(m)
if m is not None:
    print(m.group())
m = re.search('^The','end. The')
print(m)
if m is not None:
    print(m.group())
m= re.search('The$','end. The')
print(m)
if m is not None:
    print(m.group())
m = re.search('The$','The end')
print(m)
if m is not None:
    print(m.group())
# 字符串前面的r表示该字符串中的特殊字符不转义如(\b)
m =re.search(r'\bthis',"what is this")
print(m)
if m is not None:
    print(m.group())
m = re.search(r'\bthis',"what'sthis")
print(m)
if m is not None:
    print(m.group())
m = re.search(r'\bthis',"what's this")
print(m)
if m is not None:
    print(m.group())
# this的右侧是a，a也是单词，不是边界
m =re.search(r'\bthis\b',"what's thisa")
print(m)
if m is not None:
    print(m.group())