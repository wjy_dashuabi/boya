# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         11.2
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 使用search方法在一个字符串中查找模式:在一个字符串中搜索满足文本模式的字符串
import re
m = re.match('python','I love python.')
if m is not None:
    print(m.group())#在11.1中搜索到的原因是因为hello 是hello world的前缀
print(m)
m = re.search('python','I love python.')
if  m is not None:
    print(m.group())
print(m)
