# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         11.5
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
#使用ab字符集
import re
m = re.match('[ab][cd][ef][gh]','adfh')
print(m.group())
m =re.match('[ab][cd][ef][gh]','bceg')
print(m.group())
m =re.match('[ab][cd][ef][gh]','abceh')#匹配不成功的原因是因为a和b是或者关系
print(m)
m =re.match('ab[cd][ef][gh]','abceg')
print(m.group())
m = re.match('abcd|efgh','efgh')
print(m.group())
print(m)