# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         11.6
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 重复、可选和特殊字符:(*)表示字符串出现0到n次，'+'表示字符串出现1到n次，'？'表示或有a或没有a
import re
s = 'a+b+c+'
strList = ['abc','aabc','bbabc','aabbbcccxyz']
for value in strList:
   m =re.match(s,value)
   if m is not None:
       print(m.group())
   else:
       print('{}不匹配{}'.format(value,s))
print('---------------')

s = '\d{3}-[a-z]{3}'
strList =['123-abc','432-xyz','1234-xyz','ajaxdata.json-xyzabc','543-xyz^%ab']
for value in strList:
    m = re.match(s,value)
    if  m is not None:
        print(m.group())
    else:
        print('{}不匹配{}'.format(value,s))
print('-------------')
s = '[a-z]?\d+'
strList = ['1234','a123','ab432','b234abc']
for value in strList:
    m = re.match(s,value)
    if  m is not None:
        print(m.group())
    else:
        print('{}不匹配{}'.format(value,s))
print('---------------')
email ='\w+@(\w+\.)*\w+\.com'
emailList = ['abc@126.com','test@mail.geekori.com','test-abc@geekori.com','abc@geekori.com.cn']
for value in emailList:
    m=re.match(email,value)
    if m is not None:
        print(m.group())
    else:
        print('{}不匹配{}'.format(value,email))
print('---------------')
strValue = '我的Email是lining@geekori.com,请发邮件到这个邮箱'
m = re.match(email,strValue)
print(m)
email = '[a-zA-Z0-9]+@(\w+\.)*\w+\.com'
m = re.search(email,strValue)
print(m)