# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         11.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 正则表达式:通过一个文本模式来匹配一组符合条件的字符串
# 使用match方法匹配字符串
import  re
m = re.match('hello','hello')
if  m is not None :
    print(m.group())
print(m.__class__.__name__)#输出m的类名

m=re.match('hello','world')
if  m is not None:
    print(m.group())
print(m)
m = re.match('hello','hello  world')
if   m is not None:
    print(m.group())
print(m)

s = re.match('hell','hello wordld hello')
if m is not None:
    print(m.group())
print(m)#这里是寻找到是否存在，而并不是寻找一段，所以这里取最近的值而不是采用贪婪匹配