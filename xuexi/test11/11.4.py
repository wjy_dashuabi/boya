# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         11.4
# Description:
# Author:       dell
# Date:         2019/3/5
#-------------------------------------------------------------------------------
# 匹配任何单个字符：点(.)可以匹配任意一个单个字符
import  re
s = 'bin.'
m = re.match(s,'bind')
if  m is not None:
    print(m.group())
m = re.match(s,'bin')
print(m)
m = re.search(s,'<bind>')
print(m.group())
print(m)

s1= '3.14'#使用了(.)符号的文本模式字符串
s2 = '3\.14'#使用了转义符将点(.)变成了真正的字符串
m = re.match(s1,'3.14')
print(m)
m = re.match(s1,'3314')
print(m)

m = re.match(s2,'3.14')
print(m)
m = re.match(s2,'3314')
print(m)