# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         11.11
# Description:
# Author:       dell
# Date:         2019/3/11
#-------------------------------------------------------------------------------
import re
result = re.split(';','Bill;Mike;Jhon')
print(result)
result = re.split('[,;.\s]+','a,b,,d,d;x   c;d. e')
print(result)
result = re.split('[a-z]{3}-[0-9]{2}','testabc-4321productxyz-43abill')
print(result)
result = re.split('[a-z]{3}-[0-9]{2}','testabc-4321productxyz-43abill',maxsplit=1)
print(result)