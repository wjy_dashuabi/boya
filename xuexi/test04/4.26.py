# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         4.26
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/29
#-------------------------------------------------------------------------------
values = [10,40,5,76,33,2,-12]
print(len(values))
print(max(values))
print(min(values))
print(max(4,3,2,5))
print(min(6,5,4))
#如果输入print(max('abc',5,4)) 则会报错，因为字符串和数字不能比较
list= ['x',5,4]
print(min(list))