# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         test429
# Description:列表方法
# Author:       dell
# Date:         2019/ajaxdata.json/18
#-------------------------------------------------------------------------------
print('----测试append 方法----')
numbers = [1,2,3,4]
numbers.append(5)
print(numbers)
numbers.append([6,7])
print(numbers)

print('----测试clear方法----')
names = ['bill','mary','jack']
print(names)
names.clear();
print(names)

print('----测试copy方法----')
a = [1,2,3]
b = a
b[1] = 30
print(a)
aa = [1,2,3]
bb = aa.copy()
bb[1] = 30
print(aa)

print('----测试count方法----')
search = ['he','new','he','he','world','peter',[1,2,3],'ok',[1,2,3]]
print(search.count('he'))
print(search.count([1,2,3]))

print('----测试extend方法----')
a = [1,2,3]
b = [4,5,6]
a.extend(b)
print(a)
# 使用列表链接操作，效率会更低，并不建议使用
a = [1,2,3]
b = [4,5,6]
print(a+b)
# 可以使用分片赋值的方法来实现同样的效果
a = [1,2,3]
b = [4,5,6]
a [len(a):] = b
print(a)

print('----测试index方法----')
s = ['I','love','python']
print(s.index('python'))
print('xyz在列表中不存在，所以搜索时会抛出异常')

print('----测试insert方法----')
numbers = [1,2,3,4,5]
numbers[3:3] = ['four']
print(numbers)
# 使用分片赋值也可以达到效果
numbers = [1,2,3,4,5]
numbers[3:3] = ['four']
print(numbers)

print('----测试pop方法----')
numbers = [1,2,3]
print(numbers.pop())
print(numbers.pop(0))
print(numbers)

print('----测试 remove 方法----')
words = ['he','new','he','yes','bike']
words.remove('he')
print(words)

print('----测试 reverse方法----')
numbers = [1,2,3,4,5,6]
numbers.reverse()
print(numbers)

print('----测试sort方法----')
numbers = [5,4,1,7,4,2]
numbers.sort()
print(numbers)
values = [6,5,3,7,'aa','bb','cc']
x = [5,4,1,8,6]
y = x[:]
y.sort();
print(x)
print(y)
#方法2：使用sorted函数
x = [7,6,4,8,5]
y = sorted(x)
print(x)
print(y)

print(sorted('geekori'))

x=[5,4,1,7,5]
x.sort(reverse=True)
print(x)