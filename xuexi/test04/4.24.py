# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         4.24
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/29
#-------------------------------------------------------------------------------
str = 'I love you'
print('you' in str)
print('hello' in str)
names = ['Bill','Mary','John']
print('Mike' in names)
print('Mary' in names)
