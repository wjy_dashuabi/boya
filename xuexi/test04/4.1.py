# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         4.ajaxdata.json
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/26
#-------------------------------------------------------------------------------
names = ['Bill','Mike']
numbers = [1,2,3,4,5,6]
salary = [3000.0,4000.0,5000.0]
flags =[True,False,True,True]
values = [names,numbers,salary,flags,['a','b']]
for value in values:
 print( values)