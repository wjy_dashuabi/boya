# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         4.23
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/29
#-------------------------------------------------------------------------------
spaceNum = 5
i =1
linespaceNum =spaceNum
triangle = []
while linespaceNum >=0:
    leftspaceList = [ ' '] *linespaceNum
    starList = ['*'] * (2*i-1)
    rightspaceList = [' ']*linespaceNum
    linelist = leftspaceList+starList + rightspaceList
    triangle.append(linelist)
    linespaceNum -= 1
    i += 1
for line in triangle:
    print(line)