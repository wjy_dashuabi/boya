# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         3.3
# Description:
# Author:       dell
# Date:         2019/ajaxdata.json/17
#-------------------------------------------------------------------------------
a=int(input("请输入一个奇数："))
if a%2!=0:
    b=1
    while b<=a:
        c = int((a - b) / 2)
        print(" "*c,end="")
        print("*"*b,end="")
        print(" "*c)
        b+=2
    b-=4
    c+=1
    while b>=1:
        print(" "*c,end="")
        print("*"*b,end="")
        print(" "*c)
        b-=2
        c+=1
else:
    print("请输入一个奇数")

