# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         中国排名
# Description:
# Author:       dell
# Date:         2019/3/28
#-------------------------------------------------------------------------------
import requests
from bs4 import BeautifulSoup
import bs4
def getHHtmlText(url):
    try:
        r = requests.get(url,timeout = 30 )
        r.raise_for_status()#产生异常信息
        r.encoding = r.apparent_encoding#修改他的编码
        return r.text
    except:
        return ' '
def fillUnivList(ulist,html):#
    soup = BeautifulSoup(html,'自学的乱七八糟.parser')
    for tr in soup.find('tbody').children:
        if  isinstance(tr,bs4.element.Tag):#检测是否为bs4中的tag类型，其他将过滤
            tds = tr('td')
            ulist.append([tds[0].string,tds[1].string,tds[3].string])

def printUnivList(ulist,num):
    tplt = '{0:^10}\t {ajaxdata.json:{3}^10}\t {2:^10}'
    print('Suc'+str(num))
    print(tplt.format('排名','学校名称','总分',chr(12288)))
    for i in range(num):
        u = ulist[i]
        print(tplt.format(u[0],u[1],u[2],chr(12288)))
def main():
    uinfo = []
    url = 'http://www.zuihaodaxue.cn/zuihaodaxuepaiming2016.自学的乱七八糟'
    html = getHHtmlText(url)
    fillUnivList(uinfo,html)
    printUnivList(uinfo,20)
print(main())
