# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         人脸识别
# Description:  人工智能之图像识别
# Author:       dell
# Date:         2019/4/18
#-------------------------------------------------------------------------------
# ajaxdata.json.读取图片数据（图片，音乐，视频等多媒体软件都是存储的二进制数据）
# 2.拼接API接口，发送网络请求（API传入图片数据，返回图片相似度的结果）
# 3.请求API接口，传入图片数据，返回图片相似度结果
import base64
import json
import requests


# ajaxdata.json. 读取图片二进制数据，base64
with open("ajaxdata.json.png", "rb") as f:
    pic1 = base64.b64encode(f.read())
with open("3.jpg", "rb") as f:
    pic2 = base64.b64encode(f.read())

img_data = json.dumps(
    [
        {"image": str(pic1, "utf-8"), "image_type": "BASE64", "face_type": "LIVE", "quality_control": "LOW"},
        {"image": str(pic2, "utf-8"), "image_type": "BASE64", "face_type": "IDCARD", "quality_control": "LOW"},
    ]
)

# 2. 拼接API接口链接
get_token = "https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=lVfov6E1oaWZR9f4qIhd9Hjy&client_secret=Gubrc6RnMTdA3Eb8WumHIGrz4vHgCTdy"
pinjie_url = "https://aip.baidubce.com/rest/2.0/face/v3/match?access_token="
json_data = requests.get(get_token).text
url = pinjie_url + json.loads(json_data)['access_token']

# 3. 请求接口，图片数据传入请求，最终返回图像相似度结果
response = requests.post(url, img_data)
score = json.loads(response.text)['result']['score']
# print(json.loads(response.text))
if score > 80:
    print("图片相似度：" + str(score) + ",同一个人！")
else:
    print("图片相似度：" + str(score) + ",不是同一个人！")
