# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         招聘练习
# Description:
# Author:       dell
# Date:         2019/4/20
#-------------------------------------------------------------------------------
import  requests
from bs4 import BeautifulSoup
import bs4
import re
import xlwt
#使用try except函数测试错误
try:
    #请求头，网址
    head = {'ser-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
    url = 'https://search.51job.com/list/000000,000000,0000,00,9,99,%25E6%2595%25B0%25E6%258D%25AE%25E5%2588%2586%25E6%259E%2590%25E5%25B8%2588,2,ajaxdata.json.自学的乱七八糟?lang=c&stype=&postchannel=0000&workyear=99&cotype=99&degreefrom=99&jobterm=99&companysize=99&providesalary=99&lonlat=0%2C0&radius=-ajaxdata.json&ord_field=0&confirmdate=9&fromType=&dibiaoid=0&address=&line=&specialarea=00&from=&welfare='

    #使用requests获取源码
    m = requests.get(url=url, headers=head)
    m.encoding = 'gbk'
    b = m.text
except Exception as e:
    print(e)

#使用正则表达式来获取所需要的字符串
reg = re.compile(r'class="t1 ".*?<a target="_blank" title="(.*?)".*?<span class="t2"><a target="_blank" title="(.*?)".*? <span class="t3">(.*?)</span>.*? <span class="t4">(.*?)</span>.*? <span class="t5">(.*?)</span>',re.S)
items =re.findall(reg,b)

print(items)

#创建Excel表格
newTable = 'text.xls'
wb = xlwt.Workbook(encoding='utf-8')
ws = wb.add_sheet('test1')
#创建标题 其他的不太懂
headData = ['招聘职位','公司','地址','薪资','日期']
for colnum in range(0,5):
    ws.write(0,colnum,headData[colnum],xlwt.easyxf('font:bold on'))
index=1
for item in items:
    for i in range(0, 5):
        ws.write(index, i, item[i])
    index += 1
    wb.save(newTable)
print(items)