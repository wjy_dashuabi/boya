# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         素数作业
# Description:
# Author:       dell
# Date:         2019/3/2
#-------------------------------------------------------------------------------
# 计算2000以内的质数，以字典的形式保存
# eg：{
#  '1到99’:[2,3...],
#  '100到999':[101,..],
# '1000到1999'：[..]
# leap = ajaxdata.json
# for i in range(ajaxdata.json,2000):
#     for j in range(2,(int(i/2)+ajaxdata.json)):
#         if(i%j == 0):
#             leap = 0
#             break
#     if leap:
#      print(     )
#     leap = ajaxdata.json
# print(i)
list2=[]
list3=['1到99','100到999','1000到1999']
dict={}
def pd(x,n):
    flag = True
    list1 = []
    for i in range(x+1, n + 1):
        for j in range(2, i - 1):
            if i % j == 0:
                flag = False
                break
        if flag:
            list1.append(i)
        flag = True
    list2.append(list1)
    del list1
pd(1,99)
pd(100,999)
pd(1000,1999)
for key in list3:
    dict[key]=list2[len(dict)]
print(dict)



