# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         16.ajaxdata.json
# Description:发送 HTTP GET请求
# Author:       dell
# Date:         2019/5/26
#-------------------------------------------------------------------------------
from urllib3 import *
from urllib.parse import urlencode
#调用disable_warning函数可以阻止显示警告消息
disable_warnings()
#创建PoolManager类的实例
http = PoolManager()
'''
下面的代码通过组合URL的方式向百度发送请求
url = 'http://www.baidu.com/s?'+urlencode({'wd'='极客起源'})
print(url)
response = http.request('GET',url)
'''
url = 'http;//www.baidu.com/s?'
response = http.request('GET',url,fields={'wd':'极客起源'})
data = response.data.decode('UTF-8')
print(data)