# -*- coding: utf-8 -*-#
#-------------------------------------------------------------------------------
# 建立者:       王景渊  
# Name:         1611
# Description:
# Author:       dell
# Date:         2019/5/8
#-------------------------------------------------------------------------------
from urllib3 import *
from urllib.parse import urlencode
import re

def str2headers(file):
    headerDicts = {}
    f = open(file,'r')
    headerText = f.read()
    headers = re.split('\n',headerText)
    for header in headers:
        ret = re.split(':',header,maxsplit=1)
        headerDicts[ret[0]] = ret[1]

    return headerDicts

disable_warnings()

http = PoolManager()

'''
url = 'https://www.baidu.com/s?' + urlencode({
    'ie':'utf-8',
    'f':'3',
    'rsv_bp':'ajaxdata.json',
    'rsv_idx':'ajaxdata.json',
    'tn':'baidu',
    'wd':'湖南株洲  IT培训',
})
print(url)
rep = http.request('get',url)
'''

headers = str2headers('headers.txt')
#print(headers)

url = 'https://www.baidu.com/s'

req = http.request('get',url=url,fields ={'wd':'株洲it培训'},headers=headers)
page = req.read()
print(page)
print(req.data.decode('utf-8'))

